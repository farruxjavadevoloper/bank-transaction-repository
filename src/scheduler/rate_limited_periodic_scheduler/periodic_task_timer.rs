use crate::scheduler::rate_limited_periodic_scheduler::CoordinatedTask;

/// Executes the provided task at a frequency defined by the implementation of the trait
pub trait PeriodicTaskTimer {
    type Handle: PeriodicTaskTimerHandle;

    fn start(self, task: Box<dyn CoordinatedTask<Output = ()>>) -> Self::Handle;
}

pub trait PeriodicTaskTimerHandle {
    fn reset(&self);
    fn stop(self);
}
