use crate::{
    domain::data::DomainBankAccount,
    public_interface::{BankClientConfiguration, BankConfiguration, SynchronizationPolicyConfiguration},
    store::backends::spy_store::SpyStore,
    transaction_history_provider::{
        tests::{mock_dependencies::MockTransactionHistoryProviderDependencies, DummySecretStore},
        TransactionHistoryProvider,
    },
};
use std::{collections::HashSet, sync::Arc, time::Duration};

#[test]
fn initialize__constructs_one_ingestor_per_bank() {
    // Given
    let banks = BankConfiguration::test_many_with(2, 3).into_iter().collect();
    let store = Arc::new(SpyStore::default());
    let deps = MockTransactionHistoryProviderDependencies::default();
    let secret_store = Arc::new(DummySecretStore);

    // When
    let _provider = TransactionHistoryProvider::initialize(&banks, store.clone(), store, &deps, secret_store).unwrap();

    // Then
    assert_eq!(deps.get_num_ingestor_creations(), 2);
    assert_eq!(
        deps.get_ingestor_client_configs(),
        vec![BankClientConfiguration::Test1, BankClientConfiguration::Test1]
    );
}

#[test]
fn initialize__no_ingestor_constructed_given_no_banks() {
    // Given
    let banks = HashSet::new();
    let store = Arc::new(SpyStore::default());
    let deps = MockTransactionHistoryProviderDependencies::default();
    let secret_store = Arc::new(DummySecretStore);

    // When
    let _provider = TransactionHistoryProvider::initialize(&banks, store.clone(), store, &deps, secret_store).unwrap();

    // Then
    assert_eq!(deps.get_num_ingestor_creations(), 0);
}

#[test]
fn initialize__ingestor_given_correct_bank_client_configs() {
    // Given
    let expected_configs = vec![
        BankClientConfiguration::Test1,
        BankClientConfiguration::Test2,
        BankClientConfiguration::Test1,
    ];

    let banks = vec![
        BankConfiguration {
            bank: expected_configs[0].clone(),
            ..BankConfiguration::test_with(0, 3)
        },
        BankConfiguration {
            bank: expected_configs[1].clone(),
            ..BankConfiguration::test_with(1, 3)
        },
        BankConfiguration {
            bank: expected_configs[2].clone(),
            ..BankConfiguration::test_with(2, 3)
        },
    ]
    .into_iter()
    .collect();

    let store = Arc::new(SpyStore::default());
    let deps = MockTransactionHistoryProviderDependencies::default();
    let secret_store = Arc::new(DummySecretStore);

    // When
    let _provider = TransactionHistoryProvider::initialize(&banks, store.clone(), store, &deps, secret_store).unwrap();

    // Then
    assert_eq!(deps.get_num_ingestor_creations(), 3);
    assert_eq!(
        deps.get_ingestor_client_configs().iter().collect::<HashSet<_>>(),
        expected_configs.iter().collect()
    );
}

#[test]
fn initialize__no_synchronizer_constructed_given_no_banks() {
    // Given
    let banks = HashSet::new();
    let store = Arc::new(SpyStore::default());
    let deps = MockTransactionHistoryProviderDependencies::default();
    let secret_store = Arc::new(DummySecretStore);

    // When
    let _provider = TransactionHistoryProvider::initialize(&banks, store.clone(), store, &deps, secret_store).unwrap();

    // Then
    assert_eq!(deps.get_num_synchronizer_creations(), 0);
}

#[test]
fn initialize__constructs_one_synchronizer_per_bank() {
    // Given
    let banks = BankConfiguration::test_many_with(2, 3).into_iter().collect();
    let store = Arc::new(SpyStore::default());
    let deps = MockTransactionHistoryProviderDependencies::default();
    let secret_store = Arc::new(DummySecretStore);

    // When
    let _provider = TransactionHistoryProvider::initialize(&banks, store.clone(), store, &deps, secret_store).unwrap();

    // Then
    assert_eq!(deps.get_num_synchronizer_creations(), 2);
}

#[test]
fn initialize__synchronizer_is_constructed_with_correct_bank_routing_number() {
    // Given
    let banks = BankConfiguration::test_many_with(2, 3).into_iter().collect();
    let store = Arc::new(SpyStore::default());
    let deps = MockTransactionHistoryProviderDependencies::default();
    let secret_store = Arc::new(DummySecretStore);

    // When
    let _provider = TransactionHistoryProvider::initialize(&banks, store.clone(), store, &deps, secret_store).unwrap();

    // Then
    let mut created_routing_numbers: Vec<_> = deps
        .get_synchronizer_creations()
        .iter()
        .map(|s| s.routing_number.clone())
        .collect();
    created_routing_numbers.sort();
    assert_eq!(&created_routing_numbers[..], ["bank 0".to_owned(), "bank 1".to_owned()]);
}

#[test]
fn initialize__synchronizer_is_constructed_with_correct_sync_policies() {
    // Given
    let expected_sync_policies = vec![
        SynchronizationPolicyConfiguration {
            cooldown_timeout: Duration::from_millis(100),
            periodic_synchronization_interval: Duration::from_millis(200),
        },
        SynchronizationPolicyConfiguration {
            cooldown_timeout: Duration::from_millis(200),
            periodic_synchronization_interval: Duration::from_millis(300),
        },
    ];

    let banks = vec![
        BankConfiguration {
            sync_policy: expected_sync_policies[0].clone(),
            ..BankConfiguration::test_with(0, 3)
        },
        BankConfiguration {
            sync_policy: expected_sync_policies[1].clone(),
            ..BankConfiguration::test_with(1, 3)
        },
    ]
    .into_iter()
    .collect();

    let store = Arc::new(SpyStore::default());
    let deps = MockTransactionHistoryProviderDependencies::default();
    let secret_store = Arc::new(DummySecretStore);

    // When
    let _provider = TransactionHistoryProvider::initialize(&banks, store.clone(), store, &deps, secret_store).unwrap();

    // Then
    let mut created_sync_policies: Vec<_> = deps
        .get_synchronizer_creations()
        .iter()
        .map(|s| s.sync_policy.clone())
        .collect();
    created_sync_policies.sort();
    assert_eq!(&created_sync_policies[..], expected_sync_policies);
}

#[test]
fn initialize__synchronizer_is_constructed_with_correct_bank_accounts() {
    // Given
    let expected_bank_accounts_bank_0 = vec![
        "bank 0 account 0".to_string(),
        "bank 0 account 1".to_string(),
        "bank 0 account 2".to_string(),
    ];
    let expected_bank_accounts_bank_1 = vec![
        "bank 1 account 0".to_string(),
        "bank 1 account 1".to_string(),
        "bank 1 account 2".to_string(),
    ];

    let banks = vec![
        BankConfiguration {
            account_numbers: expected_bank_accounts_bank_0.clone().into_iter().collect(),
            ..BankConfiguration::test_with(0, 3)
        },
        BankConfiguration {
            account_numbers: expected_bank_accounts_bank_1.clone().into_iter().collect(),
            ..BankConfiguration::test_with(1, 3)
        },
    ]
    .into_iter()
    .collect();

    let store = Arc::new(SpyStore::default());
    let deps = MockTransactionHistoryProviderDependencies::default();
    let secret_store = Arc::new(DummySecretStore);

    // When
    let _provider = TransactionHistoryProvider::initialize(&banks, store.clone(), store, &deps, secret_store).unwrap();

    // Then
    let bank_accounts: Vec<_> = deps
        .get_synchronizer_creations()
        .iter()
        .map(|s| {
            let mut account_list: Vec<String> = s.accounts.iter().map(DomainBankAccount::get_account_number).collect();
            account_list.sort();
            account_list
        })
        .collect();
    assert_eq!(bank_accounts.len(), 2);
    assert!(bank_accounts.contains(&expected_bank_accounts_bank_0));
    assert!(bank_accounts.contains(&expected_bank_accounts_bank_1));
}

#[test]
fn initialize__synchronizer_is_constructed_with_no_bank_accounts() {
    // Given
    let banks = BankConfiguration::test_many_with(1, 0).into_iter().collect();

    let store = Arc::new(SpyStore::default());
    let deps = MockTransactionHistoryProviderDependencies::default();
    let secret_store = Arc::new(DummySecretStore);

    // When
    let _provider = TransactionHistoryProvider::initialize(&banks, store.clone(), store, &deps, secret_store).unwrap();

    // Then
    let bank_accounts: Vec<Vec<DomainBankAccount>> = deps
        .get_synchronizer_creations()
        .iter()
        .map(|s| s.accounts.clone().into_iter().collect())
        .collect();
    assert_eq!(bank_accounts, vec![vec![]]);
}
