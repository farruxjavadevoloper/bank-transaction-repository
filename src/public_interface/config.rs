mod paths;

pub mod error;

use crate::{domain::data::DomainBankAccount, public_interface::config::error::ExpectedFile};
use error::{Error, Result};
use paths::resolve_relative_path;
use serde::{Deserialize, Serialize};
use std::{
    collections::{BTreeSet, HashSet},
    path::Path,
    time::Duration,
};
use xand_banks::banks::{mcb_adapter::config::McbConfig, treasury_prime_adapter::config::TreasuryPrimeConfig};
use xand_secrets_local_file::LocalFileSecretStoreConfiguration;
use xand_secrets_vault::VaultConfiguration;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
#[serde(deny_unknown_fields, rename_all = "snake_case")]
#[allow(clippy::large_enum_variant)]
pub enum BankClientConfiguration {
    #[cfg(test)]
    Test1,
    #[cfg(test)]
    Test2,
    Mcb(McbConfig),
    Provident(TreasuryPrimeConfig),
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct BankConfiguration {
    pub routing_number: String,
    pub account_numbers: BTreeSet<String>,
    pub bank: BankClientConfiguration,
    pub sync_policy: SynchronizationPolicyConfiguration,
}

impl BankConfiguration {
    #[must_use]
    pub fn get_domain_bank_accounts(&self) -> HashSet<DomainBankAccount> {
        self.account_numbers
            .iter()
            .map(|account_number| DomainBankAccount::new(self.routing_number.clone(), account_number.clone()))
            .collect()
    }
}

#[derive(Debug, Clone, Serialize, PartialEq, Eq, Deserialize)]
#[serde(deny_unknown_fields, tag = "backend")]
pub enum StoreConfiguration {
    #[cfg(test)]
    Mock {},
    Sqlite {
        path: String,
    },
}

impl StoreConfiguration {
    fn make_paths_absolute<P: AsRef<Path>>(&self, inputs_relative_to: P) -> Result<Self> {
        Ok(match &self {
            #[cfg(test)]
            Self::Mock {} => Self::Mock {},
            Self::Sqlite { path } => Self::Sqlite {
                path: {
                    let path_buf = resolve_relative_path(path, inputs_relative_to);
                    path_buf
                        .to_str()
                        .ok_or_else(|| Error::InvalidFilePath {
                            expected_file: ExpectedFile::Sqlite,
                            path: path_buf.to_string_lossy().to_string(),
                        })?
                        .to_string()
                },
            },
        })
    }
}

/// Configuration data passed into the top-level initializer for `BankTransactionRepository`.
///
/// Explicitly NOT Serialize/Deserialize. If you are initializing BTR from a config file, use the *Data struct for
/// deserialization and then call ".load(...)".
#[derive(Debug, Clone)]
pub struct BankTransactionRepositoryConfiguration {
    pub banks: HashSet<BankConfiguration>,
    pub store: StoreConfiguration,
    pub secret_store: SecretStoreConfiguration,
}

/// Serializable and Deserializable struct for representing BTR configuration in serialized form. Provides a ".load()"
/// function on an *already-deserialized* instance to convert it into a `BankRepositoryConfiguration`
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct BankTransactionRepositoryConfigurationData {
    #[serde(serialize_with = "serde_ordered_collections::flat::sorted_serialize")]
    banks: HashSet<BankConfiguration>,
    store: StoreConfiguration,
    secret_store: SecretStoreConfiguration,
}

impl BankTransactionRepositoryConfigurationData {
    /// Translate this `BankTransactionRepositoryConfigurationData` into a `BankTransactionRepositoryConfiguration` (no
    /// "Data"). In the process, validates inputs and sanitizes paths.
    ///
    /// Relative paths in the configuration will be resolved as if they were relative to the provided `config_dir_path`.
    pub fn load<P: AsRef<Path>>(&self, config_dir_path: P) -> Result<BankTransactionRepositoryConfiguration> {
        Ok(BankTransactionRepositoryConfiguration {
            banks: self.banks.clone(),
            store: self.store.make_paths_absolute(&config_dir_path)?,
            secret_store: self.secret_store.make_paths_absolute(&config_dir_path)?,
        })
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, Ord, PartialOrd, Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct SynchronizationPolicyConfiguration {
    #[serde(with = "duration_as_seconds", rename = "cooldown_timeout_secs")]
    pub cooldown_timeout: Duration,
    #[serde(with = "duration_as_seconds", rename = "periodic_synchronization_interval_secs")]
    pub periodic_synchronization_interval: Duration,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(deny_unknown_fields, rename_all = "snake_case")]
pub enum SecretStoreConfiguration {
    Vault(VaultConfiguration),
    LocalFile(LocalFileSecretStoreConfiguration),
}

impl SecretStoreConfiguration {
    fn resolve_relative_path_as_string<P: AsRef<Path>, R: AsRef<Path>>(
        path: P,
        relative_to: R,
        make_error_from_path: fn(String) -> Error,
    ) -> Result<String> {
        let path_buf = resolve_relative_path(path, relative_to);
        path_buf
            .to_str()
            .ok_or_else(|| make_error_from_path(path_buf.to_string_lossy().to_string()))
            .map(ToString::to_string)
    }

    fn make_paths_absolute<P: AsRef<Path>>(&self, inputs_relative_to: P) -> Result<Self> {
        Ok(match &self {
            Self::Vault(config) => Self::Vault(VaultConfiguration {
                additional_https_root_certificate_files: config
                    .additional_https_root_certificate_files
                    .as_ref()
                    .map(|vec| {
                        vec.iter()
                            .map(|p| {
                                Self::resolve_relative_path_as_string(p, &inputs_relative_to, |path| {
                                    Error::InvalidFilePath {
                                        expected_file: ExpectedFile::SecretStoreCertificate,
                                        path,
                                    }
                                })
                            })
                            .collect::<Result<_>>()
                    })
                    .transpose()?,
                ..config.clone()
            }),
            Self::LocalFile(LocalFileSecretStoreConfiguration { yaml_file_path }) => {
                Self::LocalFile(LocalFileSecretStoreConfiguration {
                    yaml_file_path: Self::resolve_relative_path_as_string(
                        yaml_file_path,
                        &inputs_relative_to,
                        |path| Error::InvalidFilePath {
                            expected_file: ExpectedFile::SecretStore,
                            path,
                        },
                    )?,
                })
            }
        })
    }
}

mod duration_as_seconds {
    use std::time::Duration;

    use serde::{Deserializer, Serializer};

    pub fn deserialize<'de, D>(deserializer: D) -> Result<Duration, D::Error>
    where
        D: Deserializer<'de>,
    {
        let duration_secs: u64 = serde::Deserialize::deserialize(deserializer)?;
        Ok(Duration::from_secs(duration_secs))
    }

    pub fn serialize<S>(duration: &Duration, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let duration_secs = duration.as_secs();
        serializer.serialize_u64(duration_secs)
    }
}

#[cfg(test)]
mod tests;
