use chrono::{DateTime, Utc};
#[cfg(test)]
use mockall::automock;

#[cfg_attr(test, automock)]
pub trait Clock: Send + Sync {
    fn now(&self) -> DateTime<Utc>;
}

#[derive(Clone)]
pub struct WallClock;

impl WallClock {
    pub const fn new() -> Self {
        Self
    }
}

impl Clock for WallClock {
    fn now(&self) -> DateTime<Utc> {
        Utc::now()
    }
}

#[cfg(test)]
mod tests {
    use crate::ingestor::clock::MockClock;
    use chrono::{DateTime, Utc};

    impl MockClock {
        pub fn new_with_time(time: DateTime<Utc>) -> Self {
            let mut mock_clock = Self::new();
            mock_clock.expect_now().return_const(time);
            mock_clock
        }
    }
}
