use crate::{
    store::client::transaction_store::query::error::Error as StoreQueryClientError,
    synchronizer::synchronizer_trait::error::Error as SynchronizerError,
};
use std::error::Error as StdError;
use thiserror::Error;

pub type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Debug, Error)]
pub enum Error {
    #[error("Requested bank with routing number {} does not exist", .routing_number)]
    MissingBank { routing_number: String },
    #[error(transparent)]
    GetHistoryError(Box<dyn StdError + Send + Sync + 'static>),
}

impl From<SynchronizerError> for Error {
    fn from(err: SynchronizerError) -> Self {
        Self::GetHistoryError(Box::new(err))
    }
}

impl From<StoreQueryClientError> for Error {
    fn from(err: StoreQueryClientError) -> Self {
        Self::GetHistoryError(Box::new(err))
    }
}
