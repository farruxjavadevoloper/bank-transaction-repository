use crate::domain::data::{DomainBankAccount, DomainTransaction};
use chrono::{DateTime, Utc};

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct IngestionRecord {
    pub bank_account: DomainBankAccount,
    pub transactions: Vec<DomainTransaction>,
    pub ingestion_time: DateTime<Utc>,
}

impl IngestionRecord {
    pub fn new(
        bank_account: DomainBankAccount,
        transactions: Vec<DomainTransaction>,
        ingestion_time: DateTime<Utc>,
    ) -> Self {
        Self {
            bank_account,
            transactions,
            ingestion_time,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    impl IngestionRecord {
        pub fn test() -> Self {
            Self::test_with(vec![DomainTransaction::test()], "2021-01-09T00:00:00Z")
        }

        pub fn test_with(transactions: Vec<DomainTransaction>, ingestion_time: &str) -> Self {
            Self {
                bank_account: DomainBankAccount::test(),
                transactions,
                ingestion_time: chrono::DateTime::parse_from_rfc3339(ingestion_time).unwrap().into(),
            }
        }
    }
}
