use crate::public_interface::{
    config::{BankTransactionRepositoryConfigurationData, SecretStoreConfiguration, StoreConfiguration},
    BankClientConfiguration, BankConfiguration, SynchronizationPolicyConfiguration,
};
use assert_matches::assert_matches;
use std::time::Duration;
use xand_secrets_local_file::LocalFileSecretStoreConfiguration;

#[test]
fn load__succeeds_given_valid_config() {
    // Given a valid config data
    let expected_secret_store_config = LocalFileSecretStoreConfiguration {
        yaml_file_path: "/absolute/path/to/secrects.yaml".to_string(),
    };
    let btr_config_data = BankTransactionRepositoryConfigurationData {
        banks: [BankConfiguration {
            routing_number: "1111111".to_string(),
            account_numbers: vec!["898989".to_string(), "676767".to_string()].into_iter().collect(),
            bank: BankClientConfiguration::Test2,
            sync_policy: SynchronizationPolicyConfiguration {
                cooldown_timeout: Duration::from_millis(10000),
                periodic_synchronization_interval: Duration::from_millis(15000),
            },
        }]
        .iter()
        .cloned()
        .collect(),
        store: StoreConfiguration::Sqlite {
            path: "/absolute/path/to/file.db".to_string(),
        },
        secret_store: SecretStoreConfiguration::LocalFile(expected_secret_store_config.clone()),
    };

    // When the config data is loaded into the config struct
    let btr_config = btr_config_data.load("unused").unwrap();

    // Then
    assert_eq!(btr_config.banks, btr_config_data.banks);
    assert_eq!(btr_config.store, btr_config_data.store);
    assert_matches!(btr_config.secret_store, SecretStoreConfiguration::LocalFile(config) if config == expected_secret_store_config);
}

#[test]
fn load__translates_relative_store_path_into_absolute_path() {
    // Given a fake config source directory
    let config_dir = "/test/config/dir";
    let database_path_config_relative = "relative/path/to/file.db";

    // Given a valid config data
    let btr_config_data = BankTransactionRepositoryConfigurationData {
        banks: [BankConfiguration {
            routing_number: "1111111".to_string(),
            account_numbers: vec!["898989".to_string(), "676767".to_string()].into_iter().collect(),
            bank: BankClientConfiguration::Test2,
            sync_policy: SynchronizationPolicyConfiguration {
                cooldown_timeout: Duration::from_millis(10000),
                periodic_synchronization_interval: Duration::from_millis(15000),
            },
        }]
        .iter()
        .cloned()
        .collect(),
        store: StoreConfiguration::Sqlite {
            path: database_path_config_relative.to_string(),
        },
        secret_store: SecretStoreConfiguration::LocalFile(LocalFileSecretStoreConfiguration {
            yaml_file_path: "path/to/secrets.yaml".to_string(),
        }),
    };

    // When the config data is loaded into the config struct
    let btr_config = btr_config_data.load(config_dir).unwrap();

    // Then
    assert_eq!(
        btr_config.store,
        StoreConfiguration::Sqlite {
            path: format!("{}/{}", config_dir, database_path_config_relative)
        }
    );
}

#[test]
fn load__translates_relative_secret_file_path_into_absolute_path() {
    // Given a fake config source directory
    let config_dir = "/test/config/dir";
    let secrets_path_config_relative = "relative/path/to/secrets.yaml";

    // Given a valid config data
    let btr_config_data = BankTransactionRepositoryConfigurationData {
        banks: [BankConfiguration {
            routing_number: "1111111".to_string(),
            account_numbers: vec!["898989".to_string(), "676767".to_string()].into_iter().collect(),
            bank: BankClientConfiguration::Test2,
            sync_policy: SynchronizationPolicyConfiguration {
                cooldown_timeout: Duration::from_millis(10000),
                periodic_synchronization_interval: Duration::from_millis(15000),
            },
        }]
        .iter()
        .cloned()
        .collect(),
        store: StoreConfiguration::Sqlite {
            path: "/foo.db".to_string(),
        },
        secret_store: SecretStoreConfiguration::LocalFile(LocalFileSecretStoreConfiguration {
            yaml_file_path: secrets_path_config_relative.to_string(),
        }),
    };

    // When the config data is loaded into the config struct
    let btr_config = btr_config_data.load(config_dir).unwrap();

    // Then
    assert_matches!(
        btr_config.secret_store,
        SecretStoreConfiguration::LocalFile(c) if c == LocalFileSecretStoreConfiguration {
            yaml_file_path: format!("{}/{}", config_dir, secrets_path_config_relative)
        }
    );
}
