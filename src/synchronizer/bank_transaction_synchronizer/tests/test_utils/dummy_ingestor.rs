use crate::ingestor::{error::Error as IngestorError, Ingestor};

#[derive(Debug)]
pub struct DummyIngestor;

#[async_trait::async_trait]
impl Ingestor for DummyIngestor {
    async fn ingest(&self, _bank_account: &crate::domain::data::DomainBankAccount) -> Result<(), IngestorError> {
        unimplemented!()
    }
}
