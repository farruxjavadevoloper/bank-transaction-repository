use async_trait::async_trait;
use auto_impl::auto_impl;

/// An async thunk. Functionally equivalent to a `Fn() -> impl Future<...>`, but:
///   - Easier to express as a type (uses `async_trait` to hide the boxed+pinned future)
///   - Easier to refer to the return type, since it's an associated type
///   - Can store internal state as arbitrary fields
#[async_trait]
#[auto_impl(Box)]
pub trait CoordinatedTask: Sync + Send {
    type Output: Sync + Send + Clone;
    async fn execute(&self) -> Self::Output;
}
