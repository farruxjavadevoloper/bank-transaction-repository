use crate::store::backends::sqlite_database::db::{models::account::new_account::NewAccount, schema::account};
use diesel::{deserialize::FromSqlRow, Queryable};

pub struct StoredAccount {
    pub id: i32,
    pub account: NewAccount,
}

type DB = diesel::sqlite::Sqlite;
/// Tuple here required to match ordering of table columns as specified in schema.rs
type Row = (i32, String, String);

/// Manually impl'ing Queryable for `StoredTransaction`  allows for
/// collecting the rows after ID into the underlying struct.  
impl Queryable<account::SqlType, DB> for StoredAccount
where
    Row: FromSqlRow<account::SqlType, DB>,
{
    type Row = Row;

    fn build(row: Self::Row) -> Self {
        // By convention, values are returned in the tuple in the same order as specified in the table.
        // Order matters - especially for multiple Strings or i32 in a row.
        Self {
            id: row.0,
            account: NewAccount {
                routing_number: row.1,
                account_number: row.2,
            },
        }
    }
}
