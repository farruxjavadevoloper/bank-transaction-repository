use crate::scheduler::rate_limited_periodic_scheduler::{
    periodic_task_timer::PeriodicTaskTimerHandle, CoordinatedTask,
};
use std::sync::{Arc, Mutex};

/// A `CoordinatedTask` that holds an optional periodic timer handle and resets it whenever the inner
/// task is executed. If the timer handle option is set to None, the inner task is just executed.
pub struct PeriodicTimerResetTask<Handle: PeriodicTaskTimerHandle + Send, Task: CoordinatedTask> {
    timer_handle: Arc<Mutex<Option<Handle>>>,
    task: Task,
}

impl<Handle: PeriodicTaskTimerHandle + Send, Task: CoordinatedTask> PeriodicTimerResetTask<Handle, Task> {
    pub fn new(timer_handle: Arc<Mutex<Option<Handle>>>, task: Task) -> Self {
        Self { timer_handle, task }
    }
}

#[async_trait::async_trait]
impl<Handle: PeriodicTaskTimerHandle + Send, Task: CoordinatedTask> CoordinatedTask
    for PeriodicTimerResetTask<Handle, Task>
{
    type Output = Task::Output;

    async fn execute(&self) -> Self::Output {
        if let Some(handle) = self
            .timer_handle
            .lock()
            .expect("Could not retrieve the timer handle during periodic ingestion execution!")
            .as_ref()
        {
            handle.reset();
        }

        self.task.execute().await
    }
}
