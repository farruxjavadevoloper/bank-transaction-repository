use crate::synchronizer::bank_transaction_synchronizer::error::Error as BankTransactionSynchronizerError;
use std::error::Error as StdError;
use thiserror::Error;

pub type Result<T, E = Error> = std::result::Result<T, E>;

#[allow(clippy::enum_variant_names)]
#[derive(Debug, Error)]
pub enum Error {
    #[error(transparent)]
    SynchronizeError(Box<dyn StdError + Send + Sync + 'static>),
}

impl From<BankTransactionSynchronizerError> for Error {
    fn from(err: BankTransactionSynchronizerError) -> Self {
        Self::SynchronizeError(Box::new(err))
    }
}
