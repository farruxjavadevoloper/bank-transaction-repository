use crate::{
    domain::data::DomainTransaction,
    store::backends::sqlite_database::db::{
        models::transaction::{monetary_value::MonetaryValue, transaction_direction_value::TransactionDirectionValue},
        schema::transaction_record,
    },
};

#[derive(Debug, Clone, Insertable, PartialEq, Eq)]
#[table_name = "transaction_record"]
pub struct NewTransaction {
    pub account_id: i32,
    pub ingestion_id: i32,
    pub transaction_hash_id: String,
    pub amount: MonetaryValue,
    pub credit_or_debit: TransactionDirectionValue,
    pub metadata: String,
}

impl NewTransaction {
    pub const fn new(
        account_id: i32,
        ingestion_id: i32,
        transaction_hash_id: String,
        amount: MonetaryValue,
        credit_or_debit: TransactionDirectionValue,
        metadata: String,
    ) -> Self {
        Self {
            account_id,
            ingestion_id,
            transaction_hash_id,
            amount,
            credit_or_debit,
            metadata,
        }
    }
}

impl From<NewTransaction> for DomainTransaction {
    fn from(txn: NewTransaction) -> Self {
        Self::new(txn.amount.val(), txn.credit_or_debit.val(), txn.metadata)
    }
}
