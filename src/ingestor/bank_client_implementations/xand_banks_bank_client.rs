pub mod error;

use crate::{
    domain::data::{BankTransaction, DomainBankAccount},
    ingestor::{
        bank_client::error::{Error as BankClientError, Result as BankClientResult},
        BankClient,
    },
    public_interface::BankClientConfiguration,
};
use chrono::Duration;
use error::{Error, Result};
use std::sync::Arc;
use xand_banks::{banks::adapter_config::AdapterConfig, date_range::DateRange, BankAdapter};
use xand_secrets::SecretKeyValueStore;

const NINETY_DAYS: i64 = 90;

pub struct XandBanksBankClient {
    bank_client: Box<dyn BankAdapter>,
}

impl XandBanksBankClient {
    pub fn new(config: &BankClientConfiguration, secret_store: Arc<dyn SecretKeyValueStore>) -> Result<Self> {
        let xand_banks_config = match config {
            #[cfg(test)]
            BankClientConfiguration::Test1 | BankClientConfiguration::Test2 => unimplemented!(),
            BankClientConfiguration::Mcb(c) => AdapterConfig::Mcb(c.clone()),
            BankClientConfiguration::Provident(c) => AdapterConfig::TreasuryPrime(c.clone()),
        };

        Ok(Self {
            bank_client: xand_banks_config
                .create(secret_store)
                .map_err(|err| Error::XandBanksError(Box::new(err)))?,
        })
    }
}

#[async_trait::async_trait]
impl BankClient for XandBanksBankClient {
    #[tracing::instrument(err, skip(self), fields(from = "btr"))]
    async fn get_transaction_history(
        &self,
        bank_account: &DomainBankAccount,
    ) -> BankClientResult<Vec<BankTransaction>> {
        Ok(self
            .bank_client
            .history(
                bank_account.get_account_number().as_str(),
                DateRange::from_past_to_now(Duration::days(NINETY_DAYS))
                    .map_err(|err| BankClientError::HistoryFetchError(Box::new(err)))?,
            )
            .await
            .map_err(|err| BankClientError::HistoryFetchError(Box::new(err)))?
            .into_iter()
            .map(BankTransaction::from)
            .collect())
    }
}

#[cfg(test)]
mod tests;
