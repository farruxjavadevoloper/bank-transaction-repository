use thiserror::Error;

#[allow(clippy::enum_variant_names)]
#[derive(Debug, Error)]
pub enum Error {
    #[error("Error parsing integer representation: {}", .0)]
    IntegerParse(String),
}
