use std::path::{Path, PathBuf};

pub fn resolve_relative_path<P: AsRef<Path>, R: AsRef<Path>>(path: P, relative_to: R) -> PathBuf {
    [relative_to.as_ref(), path.as_ref()].iter().collect()
}

#[cfg(test)]
mod tests {
    use std::path::PathBuf;

    use super::resolve_relative_path;

    #[test]
    fn resolve_relative_path__relative_second_part_is_concatenated_onto_root() {
        let root = "/config/dir/";
        let file = "./foo/secrets.yaml";

        let abs = resolve_relative_path(file, root);

        assert_eq!(abs, PathBuf::from("/config/dir/foo/secrets.yaml"));
    }

    #[test]
    fn resolve_relative_path__plain_file_name_is_concatenated_onto_root() {
        let root = "/config/dir/";
        let file = "secrets.yaml";

        let abs = resolve_relative_path(file, root);

        assert_eq!(abs, PathBuf::from("/config/dir/secrets.yaml"));
    }

    #[test]
    fn resolve_relative_path__omitted_trailing_slash_in_root_dir_introduced() {
        let root = "/config/dir";
        let file = "./secrets.yaml";

        let abs = resolve_relative_path(file, root);

        assert_eq!(abs, PathBuf::from("/config/dir/secrets.yaml"));
    }

    #[test]
    fn resolve_relative_path__absolute_second_part_remains_unchanged() {
        let root = "/config/dir/";
        let file = "/some/absolute/path/secrets.yaml";

        let abs = resolve_relative_path(file, root);

        assert_eq!(abs, PathBuf::from("/some/absolute/path/secrets.yaml"));
    }

    #[test]
    fn resolve_relative_path__resolves_reference_outside_of_config_dir_tree() {
        let root = "/config/dir/";
        let file = "../../my_secrets_dir/secrets.yaml";

        let abs = resolve_relative_path(file, root);

        assert_eq!(abs, PathBuf::from("/config/dir/../../my_secrets_dir/secrets.yaml"));
    }
}
