# Bank Transaction Repository

## Developer Pre-requisites

Install diesel required dependencies by running 
```bash
./deps.sh
```

## Testing

### Unit Testing
Run with:
```bash
make unit-test
```

### Integration Testing
Run with:
```bash
make integ-test
```
If integration tests are not passing, your system may be in a weird state where the `bank-mocks` docker container
cannot be reached properly. `kill` all related containers and run `docker system prune` to clean up any hanging resources.

### MCB Production Integration Test
The test compares the credit transactions received by calling MCB's API directly with Transparent's production trust account with the transactions fetched and stored into the Trust's sqlite database. Before running it several environment variables must be set with sensitive info which can be obtained in our shared 1Password.

By default the test does not run with all the other tests. It is behind the non-default feature flag, `prod-integ`.

Run with:
```bash
export ORG_ID="redacted"
export TRUST_ACCOUNT="redacted"
export MCB_TRUST_USERNAME="redacted"
export MCB_TRUST_PASSWORD="redacted"
make prod-integ-test
```

## Database
The following are instructions for quickly setting up a sqlite database and migration with diesel.
General updates to the repo don't require these steps. Updating the existing schema and regenerating
the initial migration can be done by running `make schema`. 

**Note: the `timestamp` field in the `ingestion` table must be manually updated in the appropriate macro 
in the schema file so that the timestamp can be stored as a UNIX Timestamp (i64).**

The following steps are what we did for the initial setup and shouldn't be required for normal development.

Steps (TLDR; Follow the getting [started guide](https://diesel.rs/guides/getting-started)):

1. Specify a dummy DATABASE_URL in `.env` file, then run
    ```bash
    diesel setup
    ```
    This produces a `migrations/` dir and `diesel.toml` file.

1. Update schema path in `diesel.toml` so it's written within the sqlite module in the repo.

1. Generate an "initial_schema" migration
   ```bash
    diesel migration generate initial_schema   
   ```
1. Write sql in the new `up.sql` and `down.sql` files

1. Run migration
   ```bash
   diesel migration run
   ```
   This produces a `schema.rs` file in the specified location.

1. While modifying the SQL tables, regenerate the `schema.rs` with
   ```bash
   rm {DATABASE_URL}
   diesel migration run
   ```

## Updating database schema

### Generating migration

Diesel expects a `migrations` dir. If it doesn't exist, create it with:
```bash
mkdir migrations
```

With the given SQL schema prdouced by the `table!` macros, run 

```bash
diesel migration generate initial_schema
```

This produces a timestamped schema file in the `./migrations` dir that the application will use
to create the db on start, if it doesn't exist.

## CI / CD Artifacts

### Tag-To-Publish Stable

Push a tag in the form of v${VERSION} to release a stable version of the crate.

### Beta Artifacts

All development branches have manual jobs for publishing a beta package.


