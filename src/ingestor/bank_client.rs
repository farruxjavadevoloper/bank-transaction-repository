pub mod error;

use crate::{
    domain::data::{BankTransaction, DomainBankAccount},
    ingestor::bank_client::error::Result,
};
use async_trait::async_trait;

#[async_trait]
pub trait BankClient: Send + Sync {
    async fn get_transaction_history(&self, bank_account: &DomainBankAccount) -> Result<Vec<BankTransaction>>;

    #[cfg(test)]
    fn get_config(&self) -> crate::public_interface::BankClientConfiguration {
        unimplemented!()
    }
}

#[async_trait::async_trait]
impl BankClient for Box<dyn BankClient> {
    async fn get_transaction_history(&self, bank_account: &DomainBankAccount) -> Result<Vec<BankTransaction>> {
        self.as_ref().get_transaction_history(bank_account).await
    }

    #[cfg(test)]
    fn get_config(&self) -> crate::public_interface::BankClientConfiguration {
        self.as_ref().get_config()
    }
}
