use chrono::{DateTime, Utc};

/// An open, optionally-terminated interval of date-times, in UTC.
///
/// Represents all times later than "start" and earlier than "end". If no "end" is included, the range is said to be
/// "unterminated", and thus the range represents all times later than "start" unconditionally.
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct DateRange {
    start: DateTime<Utc>,
    end: Option<DateTime<Utc>>,
}

impl DateRange {
    pub const fn new_unterminated(start: DateTime<Utc>) -> Self {
        Self { start, end: None }
    }

    pub const fn get_start(&self) -> DateTime<Utc> {
        self.start
    }

    pub const fn get_end(&self) -> Option<DateTime<Utc>> {
        self.end
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    impl DateRange {
        pub fn test() -> Self {
            Self::test_terminated_with("2021-01-08T12:00:00Z", "2021-01-09T12:00:00Z")
        }

        pub fn test_terminated_with(start: &str, end: &str) -> Self {
            Self {
                start: chrono::DateTime::parse_from_rfc3339(start).unwrap().into(),
                end: Some(chrono::DateTime::parse_from_rfc3339(end).unwrap().into()),
            }
        }

        pub fn test_unterminated_with(start: &str) -> Self {
            Self {
                start: chrono::DateTime::parse_from_rfc3339(start).unwrap().into(),
                end: None,
            }
        }
    }
}

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum DateRangePolicy {
    /// Binds bank txn DateTime stamps in a given sync interval to previous sync Datetime stamp.
    /// This policy makes the bank txns as old as possible.
    Early,
    /// Binds bank txn DateTime stamps in a given sync interval to the sync Datetime stamp where
    /// they were discovered. This policy makes the bank txns as young as possible.
    #[cfg(test)]
    Late,
}
