use crate::{
    domain::data::DomainBankAccount,
    ingestor::IngestionHandle,
    scheduler::factory_trait::{
        error::{Error as SchedulerFactoryError, Result},
        SchedulerFactory,
    },
    synchronizer::bank_transaction_synchronizer::tests::test_utils::SpyScheduler,
};
use thiserror::Error;

#[derive(Debug, Default, Clone)]
pub struct ErroringSchedulerFactory;

impl SchedulerFactory for ErroringSchedulerFactory {
    type Scheduler = SpyScheduler;

    fn create(&self, _ingestor: IngestionHandle, _bank_account: &DomainBankAccount) -> Result<Self::Scheduler> {
        Err(SchedulerFactoryError::SchedulerCreationError(Box::new(Error::Test)))
    }
}

#[derive(Debug, Error)]
pub enum Error {
    #[error("Test Error")]
    Test,
}
