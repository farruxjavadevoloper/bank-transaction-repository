use super::{
    factory_trait::{error::Result, SchedulerFactory},
    rate_limited_periodic_scheduler::RateLimitedPeriodicScheduler,
};
use crate::{domain::data::DomainBankAccount, ingestor::IngestionHandle};
use std::time::Duration;

pub struct RateLimitedPeriodicSchedulerFactory {
    account_cooldown: Duration,
    account_automatic_synchronization_period: Duration,
}

impl RateLimitedPeriodicSchedulerFactory {
    pub const fn new(account_cooldown: Duration, account_automatic_synchronization_period: Duration) -> Self {
        Self {
            account_cooldown,
            account_automatic_synchronization_period,
        }
    }
}

impl SchedulerFactory for RateLimitedPeriodicSchedulerFactory {
    type Scheduler = RateLimitedPeriodicScheduler;

    fn create(&self, ingestor: IngestionHandle, bank_account: &DomainBankAccount) -> Result<Self::Scheduler> {
        Ok(RateLimitedPeriodicScheduler::new(
            self.account_cooldown,
            self.account_automatic_synchronization_period,
            ingestor,
            bank_account.clone(),
        ))
    }
}
