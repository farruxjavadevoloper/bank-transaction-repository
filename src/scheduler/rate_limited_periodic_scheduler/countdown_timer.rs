mod clock_countdown_timer;

pub use clock_countdown_timer::ClockCountdownTimer;

#[async_trait::async_trait]
pub trait CountdownTimer {
    /// Sets the timer to "un-expired" state, with its full period remaining.
    fn reset(&mut self);

    /// Indicates whether the timer's period has elapsed since the last reset.
    fn is_expired(&self) -> bool;

    /// Returns a Future which resolves when the timer will expire, assuming no intervening resets are applied.
    async fn wait_for_expiration(&self);
}
