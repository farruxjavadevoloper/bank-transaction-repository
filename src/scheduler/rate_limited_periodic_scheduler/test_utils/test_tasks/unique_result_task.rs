use crate::scheduler::rate_limited_periodic_scheduler::CoordinatedTask;
use async_trait::async_trait;
use std::sync::{
    atomic::{AtomicU32, Ordering},
    Arc,
};

/// A test `CoordinatedTask` that returns a unique incremented integer value upon execution.
#[derive(Debug, Default, Clone)]
pub struct UniqueResultTask {
    counter: Arc<AtomicU32>,
}

#[async_trait]
impl CoordinatedTask for UniqueResultTask {
    type Output = u32;

    async fn execute(&self) -> Self::Output {
        self.counter.fetch_add(1, Ordering::SeqCst) + 1
    }
}
