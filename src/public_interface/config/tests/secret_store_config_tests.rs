use crate::public_interface::config::{
    error::{Error, ExpectedFile},
    SecretStoreConfiguration,
};
use assert_matches::assert_matches;
use std::{os::unix::ffi::OsStrExt, path::Path};
use xand_secrets::{ExposeSecret, Secret};
use xand_secrets_local_file::LocalFileSecretStoreConfiguration;
use xand_secrets_vault::VaultConfiguration;

#[test]
fn make_paths_absolute__local_file_path_is_unchanged_when_absolute() {
    // Given
    let file_config = LocalFileSecretStoreConfiguration {
        yaml_file_path: "/absolute/path/to/secrets.yaml".to_string(),
    };
    let secret_config = SecretStoreConfiguration::LocalFile(file_config.clone());

    // When
    let result = secret_config.make_paths_absolute(Path::new("/unused/path")).unwrap();

    // Then
    assert_matches!(result, SecretStoreConfiguration::LocalFile(config) if config == file_config);
}

#[test]
fn make_paths_absolute__local_file_path_is_made_absolute_relative_to_given_path() {
    // Given
    let absolute_path_to_dir = "/absolute";
    let relative_path_to_file = "path/to/secrets.yaml";
    let secret_config = SecretStoreConfiguration::LocalFile(LocalFileSecretStoreConfiguration {
        yaml_file_path: relative_path_to_file.to_string(),
    });

    // When
    let result = secret_config
        .make_paths_absolute(Path::new(absolute_path_to_dir))
        .unwrap();

    // Then
    let expected_config = LocalFileSecretStoreConfiguration {
        yaml_file_path: format!("{}/{}", absolute_path_to_dir, relative_path_to_file),
    };
    assert_matches!(result, SecretStoreConfiguration::LocalFile(config) if config == expected_config);
}

#[test]
#[cfg(unix)]
fn make_paths_absolute__returns_error_when_given_bad_path() {
    // Given
    let bad_str_absolute_path = Path::new(std::ffi::OsStr::from_bytes(&[0x80]));
    let relative_path_to_file = "path/to/secrets.yaml";
    let secret_config = SecretStoreConfiguration::LocalFile(LocalFileSecretStoreConfiguration {
        yaml_file_path: relative_path_to_file.to_string(),
    });

    // When
    let error = secret_config
        .make_paths_absolute(Path::new(bad_str_absolute_path))
        .unwrap_err();

    // Then
    let expected_error_path = format!("{}/{}", std::char::REPLACEMENT_CHARACTER, relative_path_to_file);
    assert_matches!(error, Error::InvalidFilePath{ expected_file, path } 
        if expected_file == ExpectedFile::SecretStore && path == expected_error_path);
}

#[test]
fn make_paths_absolute__vault_config_is_unchanged_when_no_certificate_paths_are_absolute() {
    // Given
    let vault_config = VaultConfiguration {
        http_endpoint: String::from("https://example.com/secrets"),
        token: Secret::new(String::from("super-secret-token")),
        additional_https_root_certificate_files: Some(vec![
            String::from("/foo/bar/cert1.pem"),
            String::from("/abc/123/cert2.pem"),
        ]),
    };
    let secret_config = SecretStoreConfiguration::Vault(vault_config.clone());

    // When
    let result = secret_config.make_paths_absolute(Path::new("/unused/path")).unwrap();

    // Then
    assert_matches!(
        result,
        SecretStoreConfiguration::Vault(
            VaultConfiguration { http_endpoint, token, additional_https_root_certificate_files }
        ) if http_endpoint == vault_config.http_endpoint
            && token.expose_secret() == vault_config.token.expose_secret()
            && additional_https_root_certificate_files == vault_config.additional_https_root_certificate_files
    );
}

#[test]
fn make_paths_absolute__vault_certificate_path_is_made_absolute_relative_to_given_path() {
    // Given
    let absolute_path_to_dir = "/absolute";
    let relative_path_to_file = "path/to/cert.pem";
    let vault_config = VaultConfiguration {
        http_endpoint: String::from("https://example.com/secrets"),
        token: Secret::new(String::from("super-secret-token")),
        additional_https_root_certificate_files: Some(vec![String::from(relative_path_to_file)]),
    };
    let secret_config = SecretStoreConfiguration::Vault(vault_config);

    // When
    let result = secret_config
        .make_paths_absolute(Path::new(absolute_path_to_dir))
        .unwrap();

    // Then
    let expected_additional_https_root_certificate_files =
        Some(vec![format!("{}/{}", absolute_path_to_dir, relative_path_to_file)]);
    assert_matches!(result, SecretStoreConfiguration::Vault(config) if config.additional_https_root_certificate_files == expected_additional_https_root_certificate_files);
}

#[test]
fn make_paths_absolute__vault_certificate_path_is_made_absolute_relative_to_given_path_other_is_left_absolute() {
    // Given a VaultConfiguration with one relative path and one absolute path
    let absolute_path_to_dir = "/absolute";
    let relative_path_to_file = "path/to/cert.pem";
    let some_absolute_path_to_file = "/some/absolute/path/to/other_cert.pem";
    let vault_config = VaultConfiguration {
        http_endpoint: String::from("https://example.com/secrets"),
        token: Secret::new(String::from("super-secret-token")),
        additional_https_root_certificate_files: Some(vec![
            String::from(relative_path_to_file),
            String::from(some_absolute_path_to_file),
        ]),
    };
    let secret_config = SecretStoreConfiguration::Vault(vault_config);

    // When
    let result = secret_config
        .make_paths_absolute(Path::new(absolute_path_to_dir))
        .unwrap();

    // Then the relative path is made absolute and the absolute path is left unchanged
    let expected_additional_https_root_certificate_files = Some(vec![
        format!("{}/{}", absolute_path_to_dir, relative_path_to_file),
        some_absolute_path_to_file.to_string(),
    ]);
    assert_matches!(result, SecretStoreConfiguration::Vault(config) if config.additional_https_root_certificate_files == expected_additional_https_root_certificate_files);
}

#[test]
fn make_paths_absolute__vault_certificate_paths_are_made_absolute_relative_to_given_path() {
    // Given a VaultConfiguration with multiple relative paths
    let absolute_path_to_dir = "/absolute";
    let relative_path_to_file_1 = "path/to/cert.pem";
    let relative_path_to_file_2 = "path/to/other_cert.pem";
    let vault_config = VaultConfiguration {
        http_endpoint: String::from("https://example.com/secrets"),
        token: Secret::new(String::from("super-secret-token")),
        additional_https_root_certificate_files: Some(vec![
            String::from(relative_path_to_file_1),
            String::from(relative_path_to_file_2),
        ]),
    };
    let secret_config = SecretStoreConfiguration::Vault(vault_config);

    // When
    let result = secret_config
        .make_paths_absolute(Path::new(absolute_path_to_dir))
        .unwrap();

    // Then all relative paths are made absolute
    let expected_additional_https_root_certificate_files = Some(vec![
        format!("{}/{}", absolute_path_to_dir, relative_path_to_file_1),
        format!("{}/{}", absolute_path_to_dir, relative_path_to_file_2),
    ]);
    assert_matches!(result, SecretStoreConfiguration::Vault(config) if config.additional_https_root_certificate_files == expected_additional_https_root_certificate_files);
}

#[test]
#[cfg(unix)]
fn make_paths_absolute__returns_error_when_given_bad_path_for_vault_certificate() {
    // Given
    let bad_str_absolute_path = Path::new(std::ffi::OsStr::from_bytes(&[0x80]));
    let relative_path_to_file = "path/to/cert.pem";
    let vault_config = VaultConfiguration {
        http_endpoint: String::from("https://example.com/secrets"),
        token: Secret::new(String::from("super-secret-token")),
        additional_https_root_certificate_files: Some(vec![String::from(relative_path_to_file)]),
    };
    let secret_config = SecretStoreConfiguration::Vault(vault_config);

    // When
    let error = secret_config
        .make_paths_absolute(Path::new(bad_str_absolute_path))
        .unwrap_err();

    // Then
    let expected_error_path = format!("{}/{}", std::char::REPLACEMENT_CHARACTER, relative_path_to_file);
    assert_matches!(error, Error::InvalidFilePath{ expected_file, path } 
        if expected_file == ExpectedFile::SecretStoreCertificate && path == expected_error_path);
}
