use crate::test_utils::{BankAccountActor, FakeSecrets};
use bank_transaction_repository::BankAccount;
use num_traits::ToPrimitive;
use std::sync::Arc;
use xand_banks::{
    banks::mcb_adapter::{
        config::{McbClientTokenRetrievalAuthConfig, McbConfig, RawAccountInfoManagerConfig},
        CloneableMcbAdapter,
    },
    models::account::TransferRequest,
    xand_money::{Money, Usd},
    BankAdapter,
};

pub struct McbActor {
    account: BankAccount,
    mcb_client: CloneableMcbAdapter,
}

impl McbActor {
    pub fn new(account: BankAccount, url: &str) -> Self {
        // Get bank client
        let mcb_client = CloneableMcbAdapter::new(
            &McbConfig {
                url: url.parse().unwrap(),
                refresh_if_expiring_in_secs: None,
                auth: McbClientTokenRetrievalAuthConfig {
                    secret_key_username: "my-fake-metropolitan-username".to_string(),
                    secret_key_password: "my-fake-metropolitan-password".to_string(),
                },
                account_info_manager: RawAccountInfoManagerConfig {
                    secret_key_client_app_ident: "client_app_ident".to_string(),
                    secret_key_organization_id: "organization_id".to_string(),
                },
                timeout: 60,
            },
            Arc::new(FakeSecrets),
        )
        .unwrap();
        Self { account, mcb_client }
    }
}

#[async_trait::async_trait]
impl BankAccountActor for McbActor {
    async fn get_balance(&self) -> u128 {
        // Query for balance
        let bal = self
            .mcb_client
            .balance(&self.account.get_account_number())
            .await
            .unwrap();

        bal.available_balance
            .into_minor_units()
            .to_u128()
            .expect("Must convert to u128")
    }

    async fn transfer_to(&self, amount: u128, metadata: String, recipient: BankAccount) {
        self.mcb_client
            .transfer(
                TransferRequest {
                    src_account_number: self.account.get_account_number(),
                    dst_account_number: recipient.get_account_number(),
                    amount: Usd::from_u64_minor_units(amount as u64).expect("Must convert Amount to Usd"),
                },
                Some(metadata),
            )
            .await
            .expect("Failed submitting mcb bank transfer");
    }
}
