use crate::{
    domain::data::DomainBankAccount,
    scheduler::{
        factory_trait::error::Error as SchedulerFactoryError, scheduler_trait::error::Error as SchedulerError,
    },
};
use thiserror::Error;

pub type Result<T, E = Error> = std::result::Result<T, E>;

#[allow(clippy::enum_variant_names)]
#[derive(Debug, Error)]
pub enum Error {
    #[error(transparent)]
    SchedulerCreationError(#[from] SchedulerFactoryError),
    #[error(transparent)]
    SchedulerError(#[from] SchedulerError),
    #[error("Unknown account: {:?}", .0)]
    UnknownAccount(DomainBankAccount),
}
