use crate::scheduler::rate_limited_periodic_scheduler::CoordinatedTask;
use async_trait::async_trait;
use std::sync::Arc;
use tokio::sync::{Mutex, MutexGuard};

/// A test `CoordinatedTask` that can be blocked to test what happens when a task execute is requested
/// while one is already running. The inner task is guaranteed to be executed before it is blocked.
#[derive(Debug, Default, Clone)]
pub struct GatedTask<InnerTask: CoordinatedTask> {
    inner: InnerTask,
    post_execution_gate: Arc<Mutex<()>>,
}

#[async_trait]
impl<InnerTask: CoordinatedTask> CoordinatedTask for GatedTask<InnerTask>
where
    InnerTask::Output: Send,
{
    type Output = InnerTask::Output;

    async fn execute(&self) -> Self::Output {
        let result = self.inner.execute().await;
        let _guard = self.post_execution_gate.lock().await;
        result
    }
}

impl<InnerTask: CoordinatedTask> GatedTask<InnerTask> {
    pub fn new(inner: InnerTask) -> Self {
        Self {
            inner,
            post_execution_gate: Arc::default(),
        }
    }
}

pub struct GatedTaskGuard<'a>(MutexGuard<'a, ()>);

impl<InnerTask: CoordinatedTask> GatedTask<InnerTask> {
    pub async fn block_tasks(&self) -> GatedTaskGuard<'_> {
        GatedTaskGuard(self.post_execution_gate.lock().await)
    }
}
