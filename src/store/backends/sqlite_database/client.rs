use super::db::schema::{
    account::dsl::account as account_table, forensics::dsl::forensics as forensics_table,
    ingestion::dsl::ingestion as ingestion_table, transaction_record::dsl::transaction_record as transaction_table,
};
use crate::{
    domain::data::{DomainBankAccount, DomainTransaction, IngestionRecord, TimestampedTransaction},
    store::{
        backends::sqlite_database::db::{
            error::{
                database::{Error as DatabaseError, Result as DatabaseResult},
                Error,
            },
            models::{
                account::{new_account::NewAccount, stored_account::StoredAccount},
                forensics::{NewForensics, StoredForensics},
                ingestion::{
                    date_time_value::DateTimeValue, new_ingestion::NewIngestion, stored_ingestion::StoredIngestion,
                },
                transaction::{
                    monetary_value::MonetaryValue, new_transaction::NewTransaction,
                    stored_transaction::StoredTransaction, transaction_direction_value::TransactionDirectionValue,
                },
            },
            schema::{
                account,
                account::columns::{account_number, routing_number},
                forensics::{bank_provided_id, dsl::forensics, transaction_hash_id as forensic_transaction_hash_id},
                ingestion,
                ingestion::columns::timestamp,
                transaction_record::columns::transaction_hash_id,
            },
            sqlite_connection_pool::SqliteConnectionPool,
        },
        client::{
            forensics_store::ForensicsStoreClient,
            transaction_store::{
                date_range::{DateRange, DateRangePolicy},
                insert::{
                    error::{Error as InsertError, Result as InsertResult},
                    TransactionStoreInsertClient,
                },
                query::{error::Result as QueryResult, TransactionStoreQueryClient},
            },
        },
    },
};
use async_trait::async_trait;
use chrono::{DateTime, Utc};
use diesel::{dsl::max, Connection, ExpressionMethods, QueryDsl, RunQueryDsl};
use std::collections::HashMap;

mod events;

// This macro is required in order to get the id of the last account
// inserted into the table.
no_arg_sql_function!(
    last_insert_rowid,
    diesel::sql_types::Integer,
    "Represents the SQL last_insert_row() function"
);

#[derive(Clone, Debug)]
pub struct SqliteDatabaseClient {
    connection_pool: SqliteConnectionPool,
}

#[async_trait]
impl TransactionStoreInsertClient for SqliteDatabaseClient {
    #[tracing::instrument(skip_all, err, fields(from = "btr"))]
    async fn insert(&self, ingestion_record: IngestionRecord) -> InsertResult<()> {
        if let Some(last_ingestion_time) = self
            .get_last_ingest_time_db(&ingestion_record.bank_account)
            .map_err(Into::<Error>::into)?
        {
            if ingestion_record.ingestion_time < last_ingestion_time {
                return Err(InsertError::InvalidIngestionTime {
                    expected_after: last_ingestion_time,
                    given: ingestion_record.ingestion_time,
                });
            }
        }
        let account: NewAccount = ingestion_record.bank_account.clone().into();
        self.connection_pool
            .connect_and_run(|conn| {
                conn.transaction::<_, diesel::result::Error, _>(|| {
                    diesel::insert_or_ignore_into(account_table)
                        .values(&account)
                        .execute(conn)?;

                    let account_id = account_table // Match account
                        .filter(routing_number.eq(account.routing_number))
                        .filter(account_number.eq(account.account_number))
                        .select(account::id)
                        .first::<i32>(conn)?;
                    let ingestion = NewIngestion::new(account_id, DateTimeValue::new(ingestion_record.ingestion_time));
                    // Ingestion is unique when called
                    diesel::insert_into(ingestion_table).values(&ingestion).execute(conn)?;

                    let ingestion_id = diesel::select(last_insert_rowid).get_result::<i32>(conn)?;
                    for txn in ingestion_record.transactions {
                        let transaction = NewTransaction::new(
                            account_id,
                            ingestion_id,
                            txn.get_transaction_id(),
                            MonetaryValue::new(txn.get_amount()),
                            TransactionDirectionValue::new(txn.get_credit_or_debit()),
                            txn.get_metadata(),
                        );

                        diesel::insert_into(transaction_table)
                            .values(&transaction)
                            .execute(conn)?;
                    }
                    Ok(())
                })
                .map_err(|err| DatabaseError::InsertError(Box::new(err)))?;
                Ok(())
            })
            .map_err(Into::<Error>::into)?;
        Ok(())
    }
}

#[async_trait]
impl TransactionStoreQueryClient for SqliteDatabaseClient {
    #[tracing::instrument(level = "trace", skip_all, err, fields(from = "btr"))]
    async fn contains_transaction_with_id(
        &self,
        bank_account: &DomainBankAccount,
        transaction_id: &str,
    ) -> QueryResult<bool> {
        let contains = self
            .connection_pool
            .connect_and_run(|conn| {
                let join = account_table
                    .filter(routing_number.eq(bank_account.get_routing_number()))
                    .filter(account_number.eq(bank_account.get_account_number()))
                    .inner_join(transaction_table)
                    .filter(transaction_hash_id.eq(transaction_id));

                let results = join
                    .load::<(StoredAccount, StoredTransaction)>(conn)
                    .map_err(|err| DatabaseError::FetchError(Box::new(err)))?;

                Ok(!results.is_empty())
            })
            .map_err(Into::<Error>::into)?;

        Ok(contains)
    }

    #[tracing::instrument(level = "trace", skip_all, err, fields(from = "btr"))]
    async fn get_last_ingest_time(&self, bank_account: &DomainBankAccount) -> QueryResult<Option<DateTime<Utc>>> {
        Ok(self
            .get_last_ingest_time_db(bank_account)
            .map_err(Into::<Error>::into)?)
    }

    #[tracing::instrument(level = "trace", skip_all, err, fields(from = "btr"))]
    async fn get_timestamped_txns_by_id(
        &self,
        bank_account: &DomainBankAccount,
        txn_ids: Vec<&str>,
    ) -> QueryResult<HashMap<String, TimestampedTransaction>> {
        Ok(self
            .connection_pool
            .connect_and_run(|conn| {
                let join = account_table
                    .filter(routing_number.eq(bank_account.get_routing_number()))
                    .filter(account_number.eq(bank_account.get_account_number()))
                    .inner_join(ingestion_table.inner_join(transaction_table))
                    .load::<(StoredAccount, (StoredIngestion, StoredTransaction))>(conn)
                    .map_err(|err| DatabaseError::FetchError(Box::new(err)))?;

                Ok(join
                    .into_iter()
                    .filter_map(|(_account, (ingestion, txn))| {
                        let id = txn.transaction_hash_id.clone();
                        if txn_ids.contains(&id.as_ref()) {
                            let time = ingestion.timestamp();
                            Some((
                                id,
                                TimestampedTransaction {
                                    transaction: txn.into(),
                                    timestamp: time,
                                },
                            ))
                        } else {
                            None
                        }
                    })
                    .collect())
            })
            .map_err(Into::<Error>::into)?)
    }

    #[tracing::instrument(skip_all, err, fields(from = "btr"))]
    async fn query(
        &self,
        bank_account: &DomainBankAccount,
        date_range: &DateRange,
        date_range_policy: DateRangePolicy,
    ) -> QueryResult<Vec<DomainTransaction>> {
        match date_range_policy {
            DateRangePolicy::Early => {
                let txns = self
                    .connection_pool
                    .connect_and_run(|conn| {
                        // Map ingestion to timestamp for the passed bank account for previous ingestion
                        // time look up. (Can't accomplish query in a single diesel query because diesel
                        // doesn't support joining a table onto itself).
                        let ingestion_query_results: Vec<(i32, i64)> = account_table
                            .filter(routing_number.eq(bank_account.get_routing_number()))
                            .filter(account_number.eq(bank_account.get_account_number()))
                            .inner_join(ingestion_table)
                            .select((ingestion::id, timestamp))
                            .order(ingestion::id.asc())
                            .load::<(i32, i64)>(conn)
                            .map_err(|err| DatabaseError::FetchError(Box::new(err)))?;

                        let ingestion_to_prev_ts_map: HashMap<&i32, &i64> = ingestion_query_results
                            .iter()
                            .skip(1)
                            .map(|(id, _ts)| id)
                            .zip(ingestion_query_results.iter().map(|(_id, ts)| ts))
                            .collect();

                        let query_join = account_table
                            .filter(routing_number.eq(bank_account.get_routing_number()))
                            .filter(account_number.eq(bank_account.get_account_number()))
                            .inner_join(ingestion_table.inner_join(transaction_table));

                        let join_results = query_join
                            .load::<(StoredAccount, (StoredIngestion, StoredTransaction))>(conn)
                            .map_err(|err| DatabaseError::FetchError(Box::new(err)))?;

                        let txns: Vec<DomainTransaction> = join_results
                            .into_iter()
                            .filter(|(_, (ingestion_dto, _))| {
                                // Applies the early policy with a look up to the previous ingestion time.
                                ingestion_to_prev_ts_map.get(&ingestion_dto.id).map_or(
                                    false,
                                    |early_policy_timestamp| {
                                        let occurred_after_start =
                                            *early_policy_timestamp > &date_range.get_start().timestamp();
                                        let occurred_before_end = date_range
                                            .get_end()
                                            .map_or(true, |end| *early_policy_timestamp < &end.timestamp());

                                        occurred_after_start && occurred_before_end
                                    },
                                )
                            })
                            .map(|(_, (_, transaction_dto))| DomainTransaction::from(transaction_dto))
                            .collect();

                        Ok(txns)
                    })
                    .map_err(Into::<Error>::into)?;

                Ok(txns)
            }
            #[cfg(test)]
            DateRangePolicy::Late => {
                let txns = self
                    .connection_pool
                    .connect_and_run(|conn| {
                        let mut query = account_table
                            .filter(routing_number.eq(bank_account.get_routing_number()))
                            .filter(account_number.eq(bank_account.get_account_number()))
                            .inner_join(ingestion_table.inner_join(transaction_table))
                            .filter(timestamp.gt(date_range.get_start().timestamp()))
                            .into_boxed();

                        if let Some(end) = date_range.get_end() {
                            query = query.filter(timestamp.lt(end.timestamp()));
                        }

                        let results = query
                            .load::<(StoredAccount, (StoredIngestion, StoredTransaction))>(conn)
                            .map_err(|err| DatabaseError::FetchError(Box::new(err)))?;

                        let txns: Vec<DomainTransaction> = results
                            .into_iter()
                            .map(|(_, (_, transaction_dto))| DomainTransaction::from(transaction_dto))
                            .collect();

                        Ok(txns)
                    })
                    .map_err(Into::<Error>::into)?;

                Ok(txns)
            }
        }
    }
}

use crate::store::client::forensics_store::error::{Error as ForensicsError, Result as ForensicsResult};

#[async_trait]
impl ForensicsStoreClient for SqliteDatabaseClient {
    async fn insert_if_new(
        &self,
        bank_txn_id: String,
        synthetic_id: String,
        ingestion_timestamp: &DateTime<Utc>,
    ) -> ForensicsResult<bool> {
        self.connection_pool
            .connect_and_run(|conn| {
                // Step 1: Check Prior Existence
                let join = forensics_table
                    .filter(bank_provided_id.eq(&bank_txn_id))
                    .filter(forensic_transaction_hash_id.eq(&synthetic_id));

                let results = join
                    .load::<StoredForensics>(conn)
                    .map_err(|err| DatabaseError::FetchError(Box::new(err)))?;
                if results.is_empty() {
                    // Step 2: Insert if it does not exist
                    conn.transaction::<_, diesel::result::Error, _>(|| {
                        let new_forensics =
                            NewForensics::new(bank_txn_id, synthetic_id, DateTimeValue::new(*ingestion_timestamp));
                        diesel::insert_or_ignore_into(forensics)
                            .values(new_forensics)
                            .execute(conn)?;
                        Ok(())
                    })
                    .map_err(|err| DatabaseError::InsertError(Box::new(err)))?;
                    Ok(true)
                } else {
                    // There was a pre-existing record
                    Ok(false)
                }
            })
            .map_err(|e| ForensicsError::BackendError(Box::new(e)))
    }
}

#[allow(clippy::missing_panics_doc)]
#[cfg(test)]
impl SqliteDatabaseClient {
    #[must_use]
    pub fn forensics_record_count(&self, bank_txn_id: &str, synthetic_id: &str) -> usize {
        self.connection_pool
            .connect_and_run(|conn| {
                let join = forensics_table
                    .filter(bank_provided_id.eq(&bank_txn_id))
                    .filter(forensic_transaction_hash_id.eq(&synthetic_id));

                let results = join.load::<StoredForensics>(conn).unwrap();

                Ok(results.len())
            })
            .unwrap()
    }
}

impl SqliteDatabaseClient {
    #[must_use]
    pub const fn new(connection_pool: SqliteConnectionPool) -> Self {
        Self { connection_pool }
    }

    fn get_last_ingest_time_db(&self, bank_account: &DomainBankAccount) -> DatabaseResult<Option<DateTime<Utc>>> {
        let ingest_time = self.connection_pool.connect_and_run(|conn| {
            let join = account_table
                .filter(routing_number.eq(bank_account.get_routing_number()))
                .filter(account_number.eq(bank_account.get_account_number()))
                .inner_join(ingestion_table);

            let ingest_time = join
                .select(max(timestamp))
                .first::<Option<DateTimeValue>>(conn)
                .map_err(|err| DatabaseError::FetchError(Box::new(err)))?;

            Ok(ingest_time.map(|date_time_value| date_time_value.val()))
        })?;

        Ok(ingest_time)
    }

    #[cfg(any(test, feature = "prod-integ"))]
    pub fn get_transactions_for(&self, bank_account: &DomainBankAccount) -> DatabaseResult<Vec<DomainTransaction>> {
        self.connection_pool.connect_and_run(|conn| {
            let join = account_table
                .filter(routing_number.eq(bank_account.get_routing_number()))
                .filter(account_number.eq(bank_account.get_account_number()))
                .inner_join(transaction_table);

            let results = join
                .load::<(StoredAccount, StoredTransaction)>(conn)
                .map_err(|err| DatabaseError::FetchError(Box::new(err)))?;

            let txns: Vec<DomainTransaction> = results
                .into_iter()
                .map(|(_, transaction_dto)| DomainTransaction::from(transaction_dto))
                .collect();

            Ok(txns)
        })
    }
}

#[cfg(test)]
mod tests;
