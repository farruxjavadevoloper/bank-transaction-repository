mod bank_client_implementations;
mod clock;
mod differ;
mod events;

#[cfg(test)]
mod tests;

pub mod bank_client;
pub mod error;

use crate::{
    domain::data::{BankTransaction, DomainBankAccount, DomainTransaction, IngestionRecord, TimestampedTransaction},
    ingestor::differ::emit_diff_transactions,
    store::client::forensics_store::error::Error as ForensicsError,
    store::{
        client::{forensics_store::ForensicsStoreClient, transaction_store::error::Error as StoreClientError},
        TransactionStoreClient,
    },
    TransactionDirection,
};
use bank_client::BankClient;
use chrono::{DateTime, Utc};
use error::{Error, NewForensicStoreClientErrors, Result};
use futures::future::join_all;
use std::collections::BTreeMap;
use std::sync::Arc;

pub use bank_client_implementations::xand_banks_bank_client;
pub use clock::{Clock, WallClock};

use crate::ingestor::events::FailedToInsertForensic;
#[cfg(test)]
pub use bank_client_implementations::spy_bank_client;

#[cfg_attr(test, mockall::automock)]
#[async_trait::async_trait]
pub trait Ingestor: Send + Sync {
    /// Retrieves the latest transaction history for the specified account and updates the backing store accordingly.
    async fn ingest(&self, bank_account: &DomainBankAccount) -> Result<()>;

    /// A test-only function used to identify shared references to the same Ingestor.
    #[cfg(test)]
    #[allow(clippy::used_underscore_binding)]
    fn test_set_identity(&self, _identity: u32) {
        unimplemented!()
    }

    /// A test-only function used to identify shared references to the same Ingestor.
    #[cfg(test)]
    fn test_get_identity(&self) -> u32 {
        unimplemented!()
    }
}

/// An Ingestor which fetches and filters transactions, from a provided `BankClient`, to include only
/// those not present in the specified store. Then inserts the non-existant transactions into the store.
pub struct UniqueTransactionIngestor<Client, Store, Forensics, Clk>
where
    Client: BankClient,
    Store: TransactionStoreClient,
    Forensics: ForensicsStoreClient,
    Clk: Clock,
{
    client: Client,
    store: Store,
    forensics_store: Forensics,
    clock: Clk,
}

impl<Client: BankClient, Store: TransactionStoreClient, Forensics: ForensicsStoreClient, Clk: Clock>
    UniqueTransactionIngestor<Client, Store, Forensics, Clk>
{
    pub fn new(client: Client, store: Store, forensics_store: Forensics, clock: Clk) -> Self {
        Self {
            client,
            store,
            forensics_store,
            clock,
        }
    }

    async fn insert_forensics(&self, forensics: Vec<(String, String)>, ingestion_time: &DateTime<Utc>) -> Result<()> {
        let insert_results: Vec<Result<(), ForensicsError>> =
            join_all(forensics.into_iter().map(|(bank_txn_id, synthetic_id)| async move {
                let _ = self
                    .forensics_store
                    .insert_if_new(bank_txn_id.clone(), synthetic_id.clone(), ingestion_time)
                    .await
                    .map_err(|err| {
                        Self::emit_error_message(bank_txn_id, synthetic_id, *ingestion_time, &err);
                        err
                    })?;
                Ok(())
            }))
            .await;

        Self::handle_forensic_insert_errors(insert_results)?;

        Ok(())
    }

    fn handle_forensic_insert_errors(results: Vec<Result<(), ForensicsError>>) -> Result<()> {
        let errors: Vec<_> = results
            .into_iter()
            .filter(Result::is_err)
            .map(Result::unwrap_err)
            .collect();

        if !errors.is_empty() {
            let new = NewForensicStoreClientErrors(errors);
            return Err(Error::ForensicsStoreClientErrors(Arc::new(new)));
        }
        Ok(())
    }

    fn emit_error_message(bank_txn_id: String, synthetic_id: String, timestamp: DateTime<Utc>, err: &ForensicsError) {
        let event = FailedToInsertForensic {
            bank_txn_id,
            synthetic_id,
            timestamp,
            error: format!("{:?}", err),
        };
        tracing::error!(message = ?event);
    }

    ///
    /// Filters out duplicate transactions from the set to be written to the store, while logging
    /// any inconsistencies found in such duplicates.
    async fn filter(
        &self,
        txns: Vec<DomainTransaction>,
        bank_account: &DomainBankAccount,
        current_ingestion_time: DateTime<Utc>,
        txn_direction_filter: Option<TransactionDirection>,
    ) -> Result<Vec<DomainTransaction>> {
        let (unique_txns, duplicates) = get_unique_txns(txns);
        if !duplicates.is_empty() {
            tracing::warn!(message = ?events::DuplicateTransactionsInHistory{
                    transactions: duplicates.clone()
            });
        }
        let existing_pairs = self
            .store
            .get_timestamped_txns_by_id(bank_account, unique_txns.keys().map(AsRef::as_ref).collect())
            .await
            .map_err(StoreClientError::from)?;

        let mut new_vec = vec![];
        for t in unique_txns.values().cloned() {
            let id = t.get_transaction_id();
            if let Some(existing_txn) = existing_pairs.get(&id) {
                emit_diff_transactions(
                    existing_txn.clone(),
                    TimestampedTransaction {
                        transaction: t,
                        timestamp: current_ingestion_time,
                    },
                );
            } else {
                let matches_direction =
                    txn_direction_filter.map_or(true, |direction| t.get_credit_or_debit() == direction);
                if matches_direction {
                    new_vec.push(t);
                }
            }
        }
        Ok(new_vec)
    }
}

fn get_unique_txns(txns: Vec<DomainTransaction>) -> (BTreeMap<String, DomainTransaction>, Vec<DomainTransaction>) {
    let mut unique_txns = BTreeMap::new();
    let mut duplicates = Vec::new();
    for txn in txns {
        if let std::collections::btree_map::Entry::Vacant(e) = unique_txns.entry(txn.get_transaction_id()) {
            e.insert(txn);
        } else {
            duplicates.push(txn);
        }
    }
    (unique_txns, duplicates)
}

#[async_trait::async_trait]
impl<Client, Store, Forensics, Clk> Ingestor for UniqueTransactionIngestor<Client, Store, Forensics, Clk>
where
    Client: BankClient,
    Store: TransactionStoreClient,
    Forensics: ForensicsStoreClient,
    Clk: Clock,
{
    #[tracing::instrument(err, skip(self), fields(from = "btr"))]
    async fn ingest(&self, bank_account: &DomainBankAccount) -> Result<()> {
        let ingestion_time = self.clock.now();
        tracing::info!(message = ?events::IngestionStarted{
            ingestion_time: &ingestion_time,
            bank_account
        });
        let bank_txns = self.client.get_transaction_history(bank_account).await?;
        let (domain_txns, forensics): (Vec<DomainTransaction>, Vec<(String, String)>) =
            bank_txns.iter().map(domain_tx_and_forensics).unzip();

        let total = domain_txns.len();
        let filtered = self
            .filter(
                domain_txns,
                bank_account,
                ingestion_time,
                Some(TransactionDirection::Credit),
            )
            .await?;
        tracing::info!(message = ?events::BankTransactionsRetrieved{
            total,
            new: filtered.len(),
            bank_account,
        });
        let ingestion_record = IngestionRecord::new(bank_account.clone(), filtered, ingestion_time);

        self.store
            .insert(ingestion_record)
            .await
            .map_err(StoreClientError::from)?;

        self.insert_forensics(forensics, &ingestion_time).await?;

        tracing::info!(message = ?events::IngestionCompleted{
            ingestion_time: &ingestion_time,
            bank_account
        });
        Ok(())
    }
}

fn domain_tx_and_forensics(bank_tx: &BankTransaction) -> (DomainTransaction, (String, String)) {
    let domain_tx: DomainTransaction = bank_tx.into();
    let bank_id = bank_tx.bank_provided_id();
    let domain_id = domain_tx.get_transaction_id();
    (domain_tx, (bank_id, domain_id))
}

/// A shared handle to an ingestor.
pub type IngestionHandle = Arc<dyn Ingestor>;
