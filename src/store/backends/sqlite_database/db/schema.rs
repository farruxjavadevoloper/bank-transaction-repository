table! {
    account (id) {
        id -> Integer,
        routing_number -> Text,
        account_number -> Text,
    }
}

table! {
    ingestion (id) {
        id -> Integer,
        account_id -> Integer,
        timestamp -> BigInt,
    }
}

table! {
    transaction_record (id) {
        id -> Integer,
        account_id -> Integer,
        ingestion_id -> Integer,
        transaction_hash_id -> Text,
        amount -> Text,
        credit_or_debit -> Text,
        metadata -> Text,
    }
}

table! {
    forensics (id) {
        id -> Integer,
        bank_provided_id -> Text,
        transaction_hash_id -> Text,
        timestamp -> BigInt,
    }
}

joinable!(ingestion -> account (account_id));
joinable!(transaction_record -> account (account_id));
joinable!(transaction_record -> ingestion (ingestion_id));

allow_tables_to_appear_in_same_query!(account, ingestion, transaction_record,);
