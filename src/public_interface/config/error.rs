use std::fmt::{Display, Formatter};
use thiserror::Error;

pub type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Debug, Error)]
pub enum Error {
    #[error("Invalid {} file path: {:?}", .expected_file, .path)]
    InvalidFilePath { expected_file: ExpectedFile, path: String },
}

#[derive(Debug, Eq, PartialEq)]
pub enum ExpectedFile {
    Sqlite,
    SecretStore,
    SecretStoreCertificate,
}

impl Display for ExpectedFile {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let string_fmt = match self {
            Self::Sqlite => "sqlite",
            Self::SecretStore => "secret store",
            Self::SecretStoreCertificate => "secret store certificate",
        };
        f.write_str(string_fmt)
    }
}
