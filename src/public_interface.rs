mod config;
mod data;

pub mod error;

use crate::{
    periodic::{Periodic, PeriodicHandle},
    store::{
        backends::sqlite_database::{
            error::{Error as SqliteInitializationError, Result as SqliteInitializationResult},
            SqliteDatabase,
        },
        TransactionStoreClient,
    },
    transaction_history_provider::{
        history_puller::HistoryPuller, ConcreteTransactionHistoryProviderDependencies, TransactionHistoryProvider,
    },
};
use error::Result;
use std::{path::Path, sync::Arc};
use xand_secrets::SecretKeyValueStore;
use xand_secrets_local_file::LocalFileSecretKeyValueStore;
use xand_secrets_vault::VaultSecretKeyValueStore;

pub use crate::store::backends::sqlite_database::SqliteDatabaseClient;
use crate::store::client::forensics_store::ForensicsStoreClient;
pub use config::{
    BankClientConfiguration, BankConfiguration, BankTransactionRepositoryConfiguration,
    BankTransactionRepositoryConfigurationData, SecretStoreConfiguration, StoreConfiguration,
    SynchronizationPolicyConfiguration,
};
pub use data::{BankAccount, HistoryRequest, Transaction, TransactionDirection};

#[derive(Clone)]
pub struct BankTransactionRepository<T, F>
where
    T: TransactionStoreClient + Clone + 'static,
    F: ForensicsStoreClient + Clone + 'static,
{
    provider: Arc<TransactionHistoryProvider<T, F, ConcreteTransactionHistoryProviderDependencies>>,
}

impl<T: TransactionStoreClient + Clone + 'static, F: ForensicsStoreClient + Clone + 'static>
    BankTransactionRepository<T, F>
{
    #[tracing::instrument(level = "debug", err, fields(from = "btr"))]
    pub fn initialize(
        config: BankTransactionRepositoryConfiguration,
        store_client: T,
        forensics_client: F,
    ) -> Result<Self> {
        tracing::info!(message = ?events::BtrInitialized{
            config: &config
        });
        let secret_store = Self::construct_secret_store(&config.secret_store)?;
        let deps = ConcreteTransactionHistoryProviderDependencies::default();
        Ok(Self {
            provider: Arc::new(TransactionHistoryProvider::initialize(
                &config.banks,
                store_client,
                forensics_client,
                &deps,
                secret_store.into(),
            )?),
        })
    }

    #[tracing::instrument(level = "debug", skip_all, fields(from = "btr"))]
    pub fn start_periodic_synchronization(&self) -> Result<impl PeriodicHandle> {
        Ok(self.provider.start_periodic_synchronization()?)
    }

    #[tracing::instrument(level = "info", skip(self), err, fields(from = "btr"))]
    pub async fn get_history(&self, request: &HistoryRequest) -> Result<Vec<Transaction>> {
        Ok(self.provider.get_history(&request.clone().into()).await?)
    }

    fn construct_secret_store(config: &SecretStoreConfiguration) -> Result<Box<dyn SecretKeyValueStore>> {
        Ok(match config {
            SecretStoreConfiguration::LocalFile(c) => {
                Box::new(LocalFileSecretKeyValueStore::create_from_config(c.clone()))
            }
            SecretStoreConfiguration::Vault(c) => Box::new(VaultSecretKeyValueStore::create_from_config(c.clone())?),
        })
    }
}

/// An absolute file path is being required here because the implicit nature of relative
/// file paths is dangerous
pub fn construct_store_client(path: &str) -> SqliteInitializationResult<Arc<SqliteDatabaseClient>> {
    assert_path_is_absolute(path)?;
    SqliteDatabase::open_with_file(path).init().map(Arc::new)
}

fn assert_path_is_absolute(store_sqlite_db_path: &str) -> SqliteInitializationResult<()> {
    if !Path::new(store_sqlite_db_path).is_absolute() {
        return Err(SqliteInitializationError::InvalidSqliteFilePath(
            store_sqlite_db_path.to_string(),
        ));
    }
    Ok(())
}

mod events {
    use crate::BankTransactionRepositoryConfiguration;

    #[derive(Debug)]
    pub struct BtrInitialized<'a> {
        pub config: &'a BankTransactionRepositoryConfiguration,
    }
}

#[cfg(test)]
mod tests {
    use crate::{
        public_interface::assert_path_is_absolute,
        store::backends::sqlite_database::error::Error as SqliteInitializationError,
    };
    use assert_matches::assert_matches;

    #[test]
    fn assert_path_is_absolute__returns_unit_given_absolute_file_path() {
        // Given an absolute file path
        let expected_path = "/absolute/file/path";

        // When
        let result = assert_path_is_absolute(expected_path);

        // Then
        result.unwrap();
    }

    #[test]
    fn assert_path_is_absolute__returns_error_given_relative_file_path() {
        // Given a relative file path
        let expected_path = "relative/file/path";

        // When
        let error = assert_path_is_absolute(expected_path).unwrap_err();

        // Then
        assert_matches!(error, SqliteInitializationError::InvalidSqliteFilePath(path) if expected_path == path);
    }
}
