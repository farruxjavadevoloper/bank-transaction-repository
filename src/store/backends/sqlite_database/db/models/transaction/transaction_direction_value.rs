use crate::domain::data::TransactionDirection;
use diesel::{
    backend::Backend,
    deserialize,
    deserialize::FromSql,
    serialize,
    serialize::{Output, ToSql},
    sqlite::Sqlite,
};
use std::{io::Write, str::FromStr};

#[derive(Debug, Clone, PartialEq, Eq, AsExpression, FromSqlRow)]
#[sql_type = "diesel::sql_types::Text"]
pub struct TransactionDirectionValue {
    val: TransactionDirection,
}

impl TransactionDirectionValue {
    pub const fn new(val: TransactionDirection) -> Self {
        Self { val }
    }

    pub const fn val(&self) -> TransactionDirection {
        self.val
    }

    pub fn val_string(&self) -> String {
        match self.val {
            TransactionDirection::Credit => "credit".to_string(),
            TransactionDirection::Debit => "debit".to_string(),
        }
    }
}

impl FromStr for TransactionDirection {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.to_ascii_lowercase().as_str() {
            "credit" => Ok(Self::Credit),
            "debit" => Ok(Self::Debit),
            _ => Err(format!("'{}' is not a valid value for TransactionDirectionValue", s)),
        }
    }
}

impl FromSql<diesel::sql_types::Text, Sqlite> for TransactionDirectionValue {
    fn from_sql(bytes: Option<&<Sqlite as Backend>::RawValue>) -> deserialize::Result<Self> {
        let string_representation = <String as FromSql<diesel::sql_types::Text, Sqlite>>::from_sql(bytes)?;
        let transaction_direction = TransactionDirection::from_str(&string_representation)?;
        Ok(Self::new(transaction_direction))
    }
}

impl ToSql<diesel::sql_types::Text, Sqlite> for TransactionDirectionValue {
    fn to_sql<W: Write>(&self, out: &mut Output<'_, W, Sqlite>) -> serialize::Result {
        let val = self.val_string();
        <String as ToSql<diesel::sql_types::Text, Sqlite>>::to_sql(&val, out)
    }
}
