use crate::store::backends::sqlite_database::db::{
    models::ingestion::{date_time_value::DateTimeValue, new_ingestion::NewIngestion},
    schema::ingestion,
};
use chrono::{DateTime, Utc};
use diesel::{deserialize::FromSqlRow, Queryable};

pub struct StoredIngestion {
    pub id: i32,
    pub ingestion: NewIngestion,
}

impl StoredIngestion {
    pub const fn timestamp(&self) -> DateTime<Utc> {
        self.ingestion.timestamp.val()
    }
}

type DB = diesel::sqlite::Sqlite;
/// Tuple here required to match ordering of table columns as specified in schema.rs
type Row = (i32, i32, DateTimeValue);

/// Manually impl'ing Queryable for `StoredTransaction` allows for
/// collecting the rows after ID into the underlying struct.
impl Queryable<ingestion::SqlType, DB> for StoredIngestion
where
    Row: FromSqlRow<ingestion::SqlType, DB>,
{
    type Row = Row;

    fn build(row: Self::Row) -> Self {
        // By convention, values are returned in the tuple in the same order as specified in the table.
        // Order matters - especially for multiple Strings or i32 in a row.
        Self {
            id: row.0,
            ingestion: NewIngestion {
                account_id: row.1,
                timestamp: row.2,
            },
        }
    }
}
