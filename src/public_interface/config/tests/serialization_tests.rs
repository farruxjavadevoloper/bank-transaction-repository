use std::time::Duration;

use insta::assert_yaml_snapshot;

use crate::public_interface::SynchronizationPolicyConfiguration;

#[test]
fn synchronization_policy_configuration__serializes_in_seconds() {
    // Given
    let sync_policy_config = SynchronizationPolicyConfiguration {
        cooldown_timeout: Duration::from_millis(10000),
        periodic_synchronization_interval: Duration::from_millis(15000),
    };

    // Then
    assert_yaml_snapshot!(&sync_policy_config, @r###"
    ---
    cooldown_timeout_secs: 10
    periodic_synchronization_interval_secs: 15
    "###);
}

#[test]
fn synchronization_policy_configuration__truncates_inexact_seconds() {
    // Given
    let sync_policy_config = SynchronizationPolicyConfiguration {
        cooldown_timeout: Duration::from_millis(10900),
        periodic_synchronization_interval: Duration::from_millis(15200),
    };

    // Then
    assert_yaml_snapshot!(&sync_policy_config, @r###"
    ---
    cooldown_timeout_secs: 10
    periodic_synchronization_interval_secs: 15
    "###);
}
