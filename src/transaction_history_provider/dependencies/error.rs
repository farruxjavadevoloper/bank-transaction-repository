use crate::synchronizer::bank_transaction_synchronizer::error::Error as BankTransactionSynchronizerError;
use std::error::Error as StdError;
use thiserror::Error;

pub type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Debug, Error)]
pub enum Error {
    #[error(transparent)]
    SynchronizerCreationError(Box<dyn StdError + Send + Sync + 'static>),
}

impl From<BankTransactionSynchronizerError> for Error {
    fn from(err: BankTransactionSynchronizerError) -> Self {
        Self::SynchronizerCreationError(Box::new(err))
    }
}
