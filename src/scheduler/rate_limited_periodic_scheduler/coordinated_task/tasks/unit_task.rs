use crate::scheduler::rate_limited_periodic_scheduler::CoordinatedTask;

/// A `CoordinatedTask` that throws out the return value of the inner task when executed.
pub struct UnitTask<Task: CoordinatedTask> {
    task: Task,
}

impl<Task: CoordinatedTask> UnitTask<Task> {
    pub fn new(task: Task) -> Self {
        Self { task }
    }
}

#[async_trait::async_trait]
impl<Task: CoordinatedTask> CoordinatedTask for UnitTask<Task> {
    type Output = ();

    async fn execute(&self) -> Self::Output {
        self.task.execute().await;
    }
}
