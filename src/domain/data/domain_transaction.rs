use crate::domain::data::{BankTransaction, TransactionDirection};
use blake2::{Blake2b512, Digest};
use serde_json::json;
use xand_money::Usd;

#[derive(PartialEq, Eq, Clone, Debug)]
pub struct DomainTransaction {
    id: String,
    amount: Usd,
    credit_or_debit: TransactionDirection,
    metadata: String,
}

impl DomainTransaction {
    pub fn new(amount: Usd, credit_or_debit: TransactionDirection, metadata: String) -> Self {
        Self {
            id: Self::hash_id(&amount, credit_or_debit, &metadata),
            amount,
            credit_or_debit,
            metadata,
        }
    }
    pub fn get_transaction_id(&self) -> String {
        self.id.clone()
    }
    pub const fn get_amount(&self) -> Usd {
        self.amount
    }
    pub const fn get_credit_or_debit(&self) -> TransactionDirection {
        self.credit_or_debit
    }
    pub fn get_metadata(&self) -> String {
        self.metadata.clone()
    }
    /// Returns the hashed identifier for this transaction
    // **If you modify this function, the database data must be migrated or cleared first for transaction hash ids!**
    fn hash_id(amount: &Usd, credit_or_debit: TransactionDirection, metadata: &str) -> String {
        let mut hasher = Blake2b512::new();
        let consistent_string = format!("{:?}{:?}{}", amount, credit_or_debit, metadata);
        hasher.update(consistent_string);
        let output = hasher.finalize();
        base64::encode(output)
    }
}

impl From<&BankTransaction> for DomainTransaction {
    fn from(txn: &BankTransaction) -> Self {
        Self::new(txn.amount(), txn.credit_or_debit(), txn.metadata())
    }
}

impl From<BankTransaction> for DomainTransaction {
    fn from(txn: BankTransaction) -> Self {
        (&txn).into()
    }
}

impl From<DomainTransaction> for serde_json::Value {
    fn from(txn: DomainTransaction) -> Self {
        Self::Array(vec![
            (json!({
                "amount": txn.amount,
                "credit_or_debit": format!("{:?}", txn.credit_or_debit),
                "metadata": txn.metadata,
            })),
        ])
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use xand_money::Money;

    impl DomainTransaction {
        pub fn test() -> Self {
            Self::new(
                Usd::from_str("100.00").unwrap(),
                TransactionDirection::Credit,
                "metadata".to_string(),
            )
        }

        pub fn test_with_metadata(metadata: &str) -> Self {
            Self::new(
                Usd::from_str("100.00").unwrap(),
                TransactionDirection::Credit,
                metadata.to_string(),
            )
        }

        pub fn test_with_amount_only(amount: &str) -> Self {
            Self::new(
                Usd::from_str(amount).unwrap(),
                TransactionDirection::Credit,
                "metadata".to_string(),
            )
        }
    }

    #[test]
    fn hash_id__returns_expected_base64_string() {
        // given
        let transaction = DomainTransaction::new(
            Usd::from_u64_minor_units(2000u64).unwrap(),
            TransactionDirection::Credit,
            "0xuewhfljhfgwrg".to_string(),
        );
        let expected_id = "63mE80rIBWyA3OI4Ed+WxpX3/0zCRgOl5gLm0ByJ1nCsajnG/D1KfLetP8lYwfXb0LW2oHpTIp7DS7xld4duGQ==";

        // when
        let id = transaction.get_transaction_id();

        // then
        assert_eq!(expected_id, id);
    }
}
