pub mod forensics_client_impl;
pub mod transaction_client_impl;

use transaction_client_impl::SpyStoreTransactionClientCalls;

#[derive(Clone, Default, Debug)]
pub struct SpyStore {
    pub transaction_client_calls: SpyStoreTransactionClientCalls,
}
