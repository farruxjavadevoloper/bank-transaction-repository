use crate::{
    domain::data::{DomainBankAccount, DomainTransaction, IngestionRecord, TimestampedTransaction},
    store::{
        backends::sqlite_database::db::SqliteDatabase,
        client::{
            forensics_store::ForensicsStoreClient,
            transaction_store::{
                date_range::{DateRange, DateRangePolicy},
                insert::{error::Error as InsertError, TransactionStoreInsertClient},
                query::TransactionStoreQueryClient,
            },
        },
    },
};
use assert_matches::assert_matches;
use chrono::{DateTime, Utc};
const STABLE_DATETIME: &str = "2021-12-12T12:12:12.12Z";

#[tokio::test]
async fn contains_transaction_with_id__empty_db_returns_false() {
    // Given
    let client = SqliteDatabase::new_in_memory().init().unwrap();
    let bank_account = DomainBankAccount::test();
    let transaction = DomainTransaction::test();

    // When
    let res = client
        .contains_transaction_with_id(&bank_account, &transaction.get_transaction_id())
        .await
        .unwrap();

    // Then
    assert!(!res);
}

#[tokio::test]
async fn insert__is_successful_on_empty_db() {
    // Given
    let client = SqliteDatabase::new_in_memory().init().unwrap();
    let ingestion_record = IngestionRecord::test();

    // When
    client.insert(ingestion_record.clone()).await.unwrap();

    // Then
    let contains_new_transaction = client
        .contains_transaction_with_id(
            &ingestion_record.bank_account,
            &ingestion_record.transactions[0].get_transaction_id(),
        )
        .await
        .unwrap();
    assert!(contains_new_transaction);
}

#[tokio::test]
async fn insert__is_successful_on_empty_db_with_multiple_transactions() {
    // Given
    let client = SqliteDatabase::new_in_memory().init().unwrap();
    let ingestion_record = IngestionRecord::test_with(
        vec![
            DomainTransaction::test_with_metadata("0"),
            DomainTransaction::test_with_metadata("1"),
        ],
        "2021-01-09T00:00:00Z",
    );

    // When
    client.insert(ingestion_record.clone()).await.unwrap();

    // Then
    let contains_new_transaction_0 = client
        .contains_transaction_with_id(
            &ingestion_record.bank_account,
            &ingestion_record.transactions[0].get_transaction_id(),
        )
        .await
        .unwrap();
    let contains_new_transaction_1 = client
        .contains_transaction_with_id(
            &ingestion_record.bank_account,
            &ingestion_record.transactions[1].get_transaction_id(),
        )
        .await
        .unwrap();
    assert!(contains_new_transaction_0 && contains_new_transaction_1);
}

#[tokio::test]
async fn contains_transaction_with_id__returns_false_when_bank_account_does_not_match() {
    // Given
    let client = SqliteDatabase::new_in_memory().init().unwrap();
    let ingestion_record = IngestionRecord::test();
    client.insert(ingestion_record.clone()).await.unwrap();

    let new_bank_account = DomainBankAccount::new("new_routing".into(), "new_account".into());

    // When
    let res = client
        .contains_transaction_with_id(
            &new_bank_account,
            &ingestion_record.transactions[0].get_transaction_id(),
        )
        .await
        .unwrap();

    // Then
    assert!(!res);
}

#[tokio::test]
async fn insert__returns_error_on_duplicative_insert() {
    // Given
    let client = SqliteDatabase::new_in_memory().init().unwrap();
    let ingestion_record = IngestionRecord::test();

    // When
    client.insert(ingestion_record.clone()).await.unwrap();
    let res = client.insert(ingestion_record).await.unwrap_err();

    // Then
    assert_matches!(res, InsertError::BackendError(_));
}

#[tokio::test]
async fn insert__returns_error_when_two_txns_have_same_id_and_account_id() {
    let client = SqliteDatabase::new_in_memory().init().unwrap();
    let ingestion_record_1 = IngestionRecord::test_with(vec![DomainTransaction::test()], "2021-01-03T00:00:00Z");
    client.insert(ingestion_record_1).await.unwrap();
    let ingestion_record_2 = IngestionRecord::test_with(vec![DomainTransaction::test()], "2021-01-09T00:00:00Z");

    // When
    let res = client.insert(ingestion_record_2).await.unwrap_err();

    // Then
    assert_matches!(res, InsertError::BackendError(_));
}

#[tokio::test]
async fn insert__returns_error_when_ingestion_time_is_before_last_ingestion_time() {
    // Given
    let client = SqliteDatabase::new_in_memory().init().unwrap();
    let ingestion_record_1 =
        IngestionRecord::test_with(vec![DomainTransaction::test_with_metadata("1")], "2021-01-09T00:00:00Z");
    client.insert(ingestion_record_1).await.unwrap();

    let ingestion_record_2 =
        IngestionRecord::test_with(vec![DomainTransaction::test_with_metadata("2")], "2021-01-03T00:00:00Z");

    // When
    let res = client.insert(ingestion_record_2).await.unwrap_err();

    // Then
    assert_matches!(res, InsertError::InvalidIngestionTime { .. });
}

#[tokio::test]
async fn insert__two_inserts_are_successful_on_empty_db() {
    let client = SqliteDatabase::new_in_memory().init().unwrap();
    let ingestion_record_1 =
        IngestionRecord::test_with(vec![DomainTransaction::test_with_metadata("1")], "2021-01-03T00:00:00Z");
    let ingestion_record_2 =
        IngestionRecord::test_with(vec![DomainTransaction::test_with_metadata("2")], "2021-01-09T00:00:00Z");

    // When
    client.insert(ingestion_record_1.clone()).await.unwrap();
    client.insert(ingestion_record_2.clone()).await.unwrap();

    // Then
    let contains_transaction_1 = client
        .contains_transaction_with_id(
            &ingestion_record_1.bank_account,
            &ingestion_record_1.transactions[0].get_transaction_id(),
        )
        .await
        .unwrap();
    assert!(contains_transaction_1);
    let contains_transaction_2 = client
        .contains_transaction_with_id(
            &ingestion_record_2.bank_account,
            &ingestion_record_2.transactions[0].get_transaction_id(),
        )
        .await
        .unwrap();
    assert!(contains_transaction_2);
}

#[tokio::test]
async fn get_last_ingest_time__empty_db_returns_ok_none() {
    // Given
    let client = SqliteDatabase::new_in_memory().init().unwrap();
    let bank_account = DomainBankAccount::test();

    // When
    let res = client.get_last_ingest_time(&bank_account).await.unwrap();

    // Then
    assert_eq!(res, None);
}

#[tokio::test]
async fn get_last_ingest_time__returns_time_with_corresponding_account() {
    // Given
    let client = SqliteDatabase::new_in_memory().init().unwrap();
    let expected_timestamp = "2021-07-09T16:39:57+00:00".to_string();
    let ingestion_record = IngestionRecord::test_with(vec![DomainTransaction::test()], expected_timestamp.as_str());
    client.insert(ingestion_record.clone()).await.unwrap();

    // When
    let res = client
        .get_last_ingest_time(&ingestion_record.bank_account)
        .await
        .unwrap()
        .map(|date_time| DateTime::to_rfc3339(&date_time));

    // Then
    assert_eq!(res, Some(expected_timestamp));
}

#[tokio::test]
async fn get_last_ingest_time__returns_latest_time_with_corresponding_account() {
    // Given
    let client = SqliteDatabase::new_in_memory().init().unwrap();
    let old_timestamp = "2021-07-09T16:39:57+00:00";
    let ingestion_record_1 =
        IngestionRecord::test_with(vec![DomainTransaction::test_with_metadata("1")], old_timestamp);
    client.insert(ingestion_record_1.clone()).await.unwrap();

    let expected_timestamp = "2021-07-09T17:00:00+00:00".to_string();
    let ingestion_record_2 = IngestionRecord::test_with(
        vec![DomainTransaction::test_with_metadata("2")],
        expected_timestamp.as_str(),
    );
    client.insert(ingestion_record_2).await.unwrap();

    // When
    let res = client
        .get_last_ingest_time(&ingestion_record_1.bank_account)
        .await
        .unwrap()
        .map(|date_time| DateTime::to_rfc3339(&date_time));

    // Then
    assert_eq!(res, Some(expected_timestamp));
}

#[tokio::test]
async fn get_last_ingest_time__returns_none_when_no_ingestions_exist_for_requested_account() {
    // Given
    let client = SqliteDatabase::new_in_memory().init().unwrap();
    let ingestion_record = IngestionRecord::test();
    client.insert(ingestion_record).await.unwrap();

    let new_bank_account = DomainBankAccount::new("new_routing".into(), "new_account".into());

    // When
    let res = client.get_last_ingest_time(&new_bank_account).await.unwrap();

    // Then
    assert_eq!(res, None);
}
#[tokio::test]
async fn get_timestamped_txns_by_id__ingested_transactions_time_returned() {
    // Given a client
    let client = SqliteDatabase::new_in_memory().init().unwrap();
    let metadata1 = "123";
    let metadata2 = "234";
    let id_not_ingested = "4gqWtftZDm0+33kFpAMWbrY7uQhzIOBMvR4ckyYe9psCDIWKJqz3RBiADXMyfU4rf8fuoWI2NTlT0tOn8h207g==";
    let txn1 = DomainTransaction::test_with_metadata(metadata1);
    let txn1_id = txn1.get_transaction_id();
    let txn2 = DomainTransaction::test_with_metadata(metadata2);
    let txn2_id = txn2.get_transaction_id();
    // Given some transactions that will be ingested
    let txns = vec![txn1.clone(), txn2.clone()];
    // Given an ingestion time
    let time: DateTime<Utc> = chrono::DateTime::parse_from_rfc3339("2021-07-09T16:39:57Z")
        .unwrap()
        .into();
    let ingestion_record = IngestionRecord::test_with(txns, &time.to_rfc3339());
    client.insert(ingestion_record.clone()).await.unwrap();

    let bank_account = ingestion_record.bank_account.clone();

    // When we lookup times for transactions that were and were not ingested
    let txn_ids = vec![txn1_id.as_str(), txn2_id.as_str(), id_not_ingested];
    let res = client.get_timestamped_txns_by_id(&bank_account, txn_ids).await.unwrap();

    // Then
    assert_eq!(
        res.get(txn1.get_transaction_id().as_str()),
        Some(&TimestampedTransaction {
            transaction: txn1,
            timestamp: time
        })
    );
    assert_eq!(
        res.get(txn2.get_transaction_id().as_str()),
        Some(&TimestampedTransaction {
            transaction: txn2,
            timestamp: time
        })
    );
    assert_eq!(res.get(id_not_ingested), None);
}

#[tokio::test]
async fn get_timestamped_txns_by_id__ingestion_of_no_txns_yields_none() {
    // Given a client
    let client = SqliteDatabase::new_in_memory().init().unwrap();
    // Given some transactions that will be ingested
    let empty_txns = vec![];
    // Given an ingestion time
    let time: DateTime<Utc> = chrono::DateTime::parse_from_rfc3339("2021-07-09T16:39:57Z")
        .unwrap()
        .into();
    let ingestion_record = IngestionRecord::test_with(empty_txns, &time.to_rfc3339());
    client.insert(ingestion_record.clone()).await.unwrap();

    // When
    let txn_ids = vec!["123", "234", "345"];
    let bank_account = ingestion_record.bank_account.clone();
    let res = client.get_timestamped_txns_by_id(&bank_account, txn_ids).await.unwrap();

    // Then
    assert!(res.is_empty());
}

#[tokio::test]
async fn insert__serializes_and_deserializes_money_type_correctly() {
    // Given
    let client = SqliteDatabase::new_in_memory().init().unwrap();
    let ingestion_record = IngestionRecord::test();
    client.insert(ingestion_record.clone()).await.unwrap();

    // When
    let txns = client.get_transactions_for(&ingestion_record.bank_account).unwrap();

    // Then
    assert_eq!(txns[0].get_amount(), ingestion_record.transactions[0].get_amount());
}

#[tokio::test]
async fn query__returns_no_transactions_on_empty_db() {
    // Given
    let client = SqliteDatabase::new_in_memory().init().unwrap();
    let bank_account = DomainBankAccount::test();
    let date_range = DateRange::test();
    let date_range_policy = DateRangePolicy::Early;

    // When
    let txns = client
        .query(&bank_account, &date_range, date_range_policy)
        .await
        .unwrap();

    // Then
    assert!(txns.is_empty());
}

#[tokio::test]
async fn query__late_policy__returns_no_transactions_because_there_are_none_after_the_requested_start_time() {
    // Given
    let client = SqliteDatabase::new_in_memory().init().unwrap();
    let ingestion_record = IngestionRecord::test_with(vec![DomainTransaction::test()], "2021-07-09T17:00:00Z");

    let date_range = DateRange::test_terminated_with("2021-07-10T00:00:00Z", "2021-07-11T00:00:00Z");
    let date_range_policy = DateRangePolicy::Late;

    client.insert(ingestion_record.clone()).await.unwrap();

    // When
    let txns = client
        .query(&ingestion_record.bank_account, &date_range, date_range_policy)
        .await
        .unwrap();

    // Then
    assert!(txns.is_empty());
}

#[tokio::test]
async fn query__late_policy__returns_no_transactions_because_there_are_none_before_the_requested_end_time() {
    // Given
    let client = SqliteDatabase::new_in_memory().init().unwrap();
    let ingestion_record = IngestionRecord::test_with(vec![DomainTransaction::test()], "2021-07-12T17:00:00Z");

    let date_range = DateRange::test_terminated_with("2021-07-10T00:00:00Z", "2021-07-11T00:00:00Z");
    let date_range_policy = DateRangePolicy::Late;

    client.insert(ingestion_record.clone()).await.unwrap();

    // When
    let txns = client
        .query(&ingestion_record.bank_account, &date_range, date_range_policy)
        .await
        .unwrap();

    // Then
    assert!(txns.is_empty());
}

#[tokio::test]
async fn query__late_policy__returns_transaction_after_start_time_when_given_no_end_time() {
    // Given
    let client = SqliteDatabase::new_in_memory().init().unwrap();
    let ingestion_record = IngestionRecord::test_with(vec![DomainTransaction::test()], "2021-07-12T17:00:00Z");

    let date_range = DateRange::test_unterminated_with("2021-07-10T00:00:00Z");
    let date_range_policy = DateRangePolicy::Late;

    client.insert(ingestion_record.clone()).await.unwrap();

    // When
    let txns = client
        .query(&ingestion_record.bank_account, &date_range, date_range_policy)
        .await
        .unwrap();

    // Then
    assert_eq!(txns.len(), 1);
    assert_eq!(txns, ingestion_record.transactions);
}

#[tokio::test]
async fn query__late_policy__returns_transactions_within_the_date_range() {
    // Given
    let client = SqliteDatabase::new_in_memory().init().unwrap();
    let ingestion_record = IngestionRecord::test_with(vec![DomainTransaction::test()], "2021-07-10T17:00:00Z");

    let date_range = DateRange::test_terminated_with("2021-07-10T00:00:00Z", "2021-07-11T00:00:00Z");
    let date_range_policy = DateRangePolicy::Late;

    client.insert(ingestion_record.clone()).await.unwrap();

    // When
    let txns = client
        .query(&ingestion_record.bank_account, &date_range, date_range_policy)
        .await
        .unwrap();

    // Then
    assert_eq!(txns.len(), 1);
    assert_eq!(txns, ingestion_record.transactions);
}

#[tokio::test]
async fn query__late_policy__returns_no_transactions_against_other_bank_account() {
    // Given
    let client = SqliteDatabase::new_in_memory().init().unwrap();
    let ingestion_record = IngestionRecord::test_with(vec![DomainTransaction::test()], "2021-07-10T17:00:00Z");

    let date_range = DateRange::test_terminated_with("2021-07-10T00:00:00Z", "2021-07-11T00:00:00Z");
    let date_range_policy = DateRangePolicy::Late;

    client.insert(ingestion_record.clone()).await.unwrap();

    let other_bank_account = DomainBankAccount::new("other_routing".into(), "other_account".into());

    // When
    let txns = client
        .query(&other_bank_account, &date_range, date_range_policy)
        .await
        .unwrap();

    // Then
    assert!(txns.is_empty());
}

#[tokio::test]
async fn query__early_policy__returns_no_transactions_because_there_are_none_after_the_requested_start_time() {
    // Given
    let client = SqliteDatabase::new_in_memory().init().unwrap();
    let ingestion_record = IngestionRecord::test_with(vec![DomainTransaction::test()], "2021-07-10T17:00:00Z");

    let date_range = DateRange::test_terminated_with("2021-07-10T00:00:00Z", "2021-07-11T00:00:00Z");
    let date_range_policy = DateRangePolicy::Early;

    client.insert(ingestion_record.clone()).await.unwrap();

    // When
    let txns = client
        .query(&ingestion_record.bank_account, &date_range, date_range_policy)
        .await
        .unwrap();

    // Then
    assert!(txns.is_empty());
}

#[tokio::test]
async fn query__early_policy__returns_transactions_within_the_date_range() {
    // Given
    let client = SqliteDatabase::new_in_memory().init().unwrap();
    let ingestion_record_1 =
        IngestionRecord::test_with(vec![DomainTransaction::test_with_metadata("1")], "2021-07-10T17:00:00Z");
    client.insert(ingestion_record_1.clone()).await.unwrap();

    let ingestion_record_2 =
        IngestionRecord::test_with(vec![DomainTransaction::test_with_metadata("2")], "2021-07-11T17:00:00Z");
    client.insert(ingestion_record_2.clone()).await.unwrap();

    let date_range = DateRange::test_terminated_with("2021-07-10T00:00:00Z", "2021-07-11T00:00:00Z");
    let date_range_policy = DateRangePolicy::Early;

    // When
    let txns = client
        .query(&ingestion_record_1.bank_account, &date_range, date_range_policy)
        .await
        .unwrap();

    // Then
    assert_eq!(txns.len(), 1);
    assert_eq!(txns, ingestion_record_2.transactions);
}

#[tokio::test]
async fn query__early_policy__returns_no_transactions_because_there_are_none_before_the_requested_end_time() {
    // Given
    let client = SqliteDatabase::new_in_memory().init().unwrap();
    let ingestion_record_1 =
        IngestionRecord::test_with(vec![DomainTransaction::test_with_metadata("1")], "2021-07-11T17:00:00Z");
    client.insert(ingestion_record_1.clone()).await.unwrap();

    let ingestion_record_2 =
        IngestionRecord::test_with(vec![DomainTransaction::test_with_metadata("2")], "2021-07-12T17:00:00Z");
    client.insert(ingestion_record_2).await.unwrap();

    let date_range = DateRange::test_terminated_with("2021-07-10T00:00:00Z", "2021-07-11T00:00:00Z");
    let date_range_policy = DateRangePolicy::Early;

    // When
    let txns = client
        .query(&ingestion_record_1.bank_account, &date_range, date_range_policy)
        .await
        .unwrap();

    // Then
    assert!(txns.is_empty());
}

#[tokio::test]
async fn query__early_policy__returns_transactions_after_start_time_when_given_no_end_time() {
    // Given
    let client = SqliteDatabase::new_in_memory().init().unwrap();
    let ingestion_record_1 =
        IngestionRecord::test_with(vec![DomainTransaction::test_with_metadata("1")], "2021-07-11T17:00:00Z");
    client.insert(ingestion_record_1.clone()).await.unwrap();

    let ingestion_record_2 =
        IngestionRecord::test_with(vec![DomainTransaction::test_with_metadata("2")], "2021-07-12T17:00:00Z");
    client.insert(ingestion_record_2.clone()).await.unwrap();

    let date_range = DateRange::test_unterminated_with("2021-07-10T00:00:00Z");
    let date_range_policy = DateRangePolicy::Early;

    // When
    let txns = client
        .query(&ingestion_record_1.bank_account, &date_range, date_range_policy)
        .await
        .unwrap();

    // Then
    assert_eq!(txns.len(), 1);
    assert_eq!(txns, ingestion_record_2.transactions);
}

#[tokio::test]
async fn query__early_policy__returns_no_transactions_against_other_bank_account() {
    // Given
    let client = SqliteDatabase::new_in_memory().init().unwrap();
    let ingestion_record_1 =
        IngestionRecord::test_with(vec![DomainTransaction::test_with_metadata("1")], "2021-07-10T17:00:00Z");
    client.insert(ingestion_record_1).await.unwrap();

    let ingestion_record_2 =
        IngestionRecord::test_with(vec![DomainTransaction::test_with_metadata("2")], "2021-07-11T17:00:00Z");
    client.insert(ingestion_record_2).await.unwrap();

    let date_range = DateRange::test_terminated_with("2021-07-10T00:00:00Z", "2021-07-11T00:00:00Z");
    let date_range_policy = DateRangePolicy::Early;

    let other_bank_account = DomainBankAccount::new("other_routing".into(), "other_account".into());

    // When
    let txns = client
        .query(&other_bank_account, &date_range, date_range_policy)
        .await
        .unwrap();

    // Then
    assert!(txns.is_empty());
}

#[tokio::test]
async fn query__early_policy__returns_transactions_after_second_ingestion_when_initial_ingestion_is_empty() {
    // Given
    let client = SqliteDatabase::new_in_memory().init().unwrap();
    let ingestion_record_1 = IngestionRecord::test_with(vec![], "2021-07-11T17:00:00Z");
    client.insert(ingestion_record_1.clone()).await.unwrap();

    let ingestion_record_2 =
        IngestionRecord::test_with(vec![DomainTransaction::test_with_metadata("1")], "2021-07-12T17:00:00Z");
    client.insert(ingestion_record_2.clone()).await.unwrap();

    let date_range = DateRange::test_unterminated_with("2021-07-10T00:00:00Z");
    let date_range_policy = DateRangePolicy::Early;

    // When
    let txns = client
        .query(&ingestion_record_1.bank_account, &date_range, date_range_policy)
        .await
        .unwrap();

    // Then
    assert_eq!(txns.len(), 1);
    assert_eq!(txns, ingestion_record_2.transactions);
}

#[tokio::test]
async fn query__early_policy__returns_transactions_ingestion_after_empty_ingestion() {
    // Given
    let client = SqliteDatabase::new_in_memory().init().unwrap();
    let ingestion_record_1 =
        IngestionRecord::test_with(vec![DomainTransaction::test_with_metadata("1")], "2021-07-11T17:00:00Z");
    client.insert(ingestion_record_1.clone()).await.unwrap();

    let ingestion_record_2 = IngestionRecord::test_with(vec![], "2021-07-12T17:00:00Z");
    client.insert(ingestion_record_2.clone()).await.unwrap();

    let ingestion_record_3 =
        IngestionRecord::test_with(vec![DomainTransaction::test_with_metadata("2")], "2021-07-13T17:00:00Z");
    client.insert(ingestion_record_3.clone()).await.unwrap();

    let date_range = DateRange::test_unterminated_with("2021-07-12T00:00:00Z");
    let date_range_policy = DateRangePolicy::Early;

    // When
    let txns = client
        .query(&ingestion_record_1.bank_account, &date_range, date_range_policy)
        .await
        .unwrap();

    // Then
    assert_eq!(txns.len(), 1);
    assert_eq!(txns, ingestion_record_3.transactions);
}

#[tokio::test]
async fn query__early_policy__returns_transactions_ingestion_after_other_bank_account_ingestion() {
    // Given
    let client = SqliteDatabase::new_in_memory().init().unwrap();

    let bank_account_other = DomainBankAccount::new("123".to_string(), "abc".to_string());

    let ingestion_record_1 =
        IngestionRecord::test_with(vec![DomainTransaction::test_with_metadata("1")], "2021-07-11T17:00:00Z");
    client.insert(ingestion_record_1.clone()).await.unwrap();

    let ingestion_record_2 = IngestionRecord::new(
        bank_account_other,
        vec![DomainTransaction::test_with_metadata("2")],
        chrono::DateTime::parse_from_rfc3339("2021-07-12T17:00:00Z")
            .unwrap()
            .into(),
    );
    client.insert(ingestion_record_2.clone()).await.unwrap();

    let ingestion_record_3 =
        IngestionRecord::test_with(vec![DomainTransaction::test_with_metadata("3")], "2021-07-13T17:00:00Z");
    client.insert(ingestion_record_3.clone()).await.unwrap();

    let date_range = DateRange::test_unterminated_with("2021-07-11T00:00:00Z");
    let date_range_policy = DateRangePolicy::Early;

    // When
    let txns = client
        .query(&ingestion_record_1.bank_account, &date_range, date_range_policy)
        .await
        .unwrap();

    // Then
    assert_eq!(txns.len(), 1);
    assert_eq!(txns, ingestion_record_3.transactions);
}

#[tokio::test]
async fn get_timestamped_txns_by_id__returns_transaction_when_it_was_ingested() {
    // Given a bank with an account and a transaction
    let client = SqliteDatabase::new_in_memory().init().unwrap();
    let ingestion_record = IngestionRecord::test();
    let expected_txn = ingestion_record.transactions[0].clone();
    let txn_id = expected_txn.clone().get_transaction_id();
    client.insert(ingestion_record.clone()).await.unwrap();

    // When
    let txns = client
        .get_timestamped_txns_by_id(&ingestion_record.bank_account, vec![txn_id.as_ref()])
        .await
        .unwrap();

    // Then
    assert_eq!(txns.len(), 1);
    assert_eq!(expected_txn, txns.get(&txn_id).unwrap().transaction);
}

#[tokio::test]
async fn get_timestamped_txns_by_id__returns_empty_collection_when_transaction_wasnt_ingested() {
    // Given a bank with an account and two transactions
    let client = SqliteDatabase::new_in_memory().init().unwrap();
    let ingested_txn = DomainTransaction::test_with_metadata("0");
    let uningested_txn = DomainTransaction::test_with_metadata("1");

    let ingestion_record = IngestionRecord::test_with(vec![ingested_txn], "2021-07-11T17:00:00Z");
    // Given that a transaction is ingested
    client.insert(ingestion_record.clone()).await.unwrap();

    let bank_acct = ingestion_record.bank_account;
    // When requesting a transaction that was not ingested
    let txns = client
        .get_timestamped_txns_by_id(&bank_acct, vec![uningested_txn.get_transaction_id().as_ref()])
        .await
        .unwrap();

    // Then
    assert!(txns.is_empty());
}

#[tokio::test]
async fn get_timestamped_txns_by_id__returns_transactions_that_match() {
    // Given a bank with an account and transactions
    let client = SqliteDatabase::new_in_memory().init().unwrap();
    // All transactions have the same bank account
    let ingested_txn1 = DomainTransaction::test_with_metadata("111");
    let ingested_txn2 = DomainTransaction::test_with_metadata("222");
    let uningested_txn1 = DomainTransaction::test_with_metadata("333");
    let uningested_txn2 = DomainTransaction::test_with_metadata("444");

    // Given only some transactions are ingested
    let ingestion_record1 = IngestionRecord::test_with(vec![ingested_txn1.clone()], "2021-07-10T17:00:00Z");
    let ingestion_record2 = IngestionRecord::test_with(vec![ingested_txn2.clone()], "2021-07-11T17:00:00Z");
    client.insert(ingestion_record1.clone()).await.unwrap();
    client.insert(ingestion_record2.clone()).await.unwrap();

    let bank_acct = ingestion_record1.bank_account;
    // When requesting transactions from all ids (ingested and un-ingested)
    let txns = client
        .get_timestamped_txns_by_id(
            &bank_acct,
            vec![
                &ingested_txn1.get_transaction_id(),
                &ingested_txn2.get_transaction_id(),
                &uningested_txn1.get_transaction_id(),
                &uningested_txn2.get_transaction_id(),
            ],
        )
        .await
        .unwrap()
        .into_iter()
        .map(
            |(
                _id,
                TimestampedTransaction {
                    transaction,
                    timestamp: _,
                },
            )| transaction,
        )
        .collect::<Vec<_>>();

    // Then we get back only the transactions that have been ingested
    assert!(txns.contains(&ingested_txn1));
    assert!(txns.contains(&ingested_txn2));
    assert!(!txns.contains(&uningested_txn1));
    assert!(!txns.contains(&uningested_txn2));
}

#[tokio::test]
async fn insert_if_new__adds_when_it_doesnt_exist() {
    //Given
    let client = SqliteDatabase::new_in_memory().init().unwrap();
    //Initialize database store (empty)
    let bank_transaction_id = "something".to_string();
    let domain_transaction_id = "something-else".to_string();
    let transaction_time = DateTime::parse_from_rfc3339(STABLE_DATETIME).unwrap().into();

    //When
    // We try run insert if new to add something that doesn't exist in current DB
    client
        .insert_if_new(
            bank_transaction_id.clone(),
            domain_transaction_id.clone(),
            &transaction_time,
        )
        .await
        .unwrap();

    //Then
    // We expect to see the insert succeed and the DB reflect the addition
    let added_count = client.forensics_record_count(bank_transaction_id.as_str(), domain_transaction_id.as_str());
    assert_eq!(added_count, 1);
}

#[tokio::test]
async fn insert_if_new__returns_true_when_new_record_added() {
    //Given
    let client = SqliteDatabase::new_in_memory().init().unwrap();
    //Initialize database store (empty)
    let bank_transaction_id = "something".to_string();
    let domain_transaction_id = "something-else".to_string();
    let transaction_time = DateTime::parse_from_rfc3339(STABLE_DATETIME).unwrap().into();

    //When
    // We try run insert if new to add something that doesn't exist in current DB
    let was_added = client
        .insert_if_new(
            bank_transaction_id.clone(),
            domain_transaction_id.clone(),
            &transaction_time,
        )
        .await
        .unwrap();

    //Then
    // We expect that the client will return positive to indicate a record was added
    assert!(was_added);
}

#[tokio::test]
async fn insert_if_new__when_adding_an_existing_forensic_then_returns_false() {
    //Given
    let client = SqliteDatabase::new_in_memory().init().unwrap();
    //Initialize database store (empty)
    let bank_transaction_id = "something".to_string();
    let domain_transaction_id = "something-else".to_string();
    let transaction_time = DateTime::parse_from_rfc3339(STABLE_DATETIME).unwrap().into();
    client
        .insert_if_new(
            bank_transaction_id.clone(),
            domain_transaction_id.clone(),
            &transaction_time,
        )
        .await
        .unwrap();

    //When
    // When we try to insert an existing forensic
    let was_inserted = client
        .insert_if_new(
            bank_transaction_id.clone(),
            domain_transaction_id.clone(),
            &transaction_time,
        )
        .await
        .unwrap();

    //Then
    // The was_inserted should return false as it does not insert a duplicate forensic
    assert!(!was_inserted);
}

#[tokio::test]
async fn insert_if_new__duplicate_records_are_not_added_to_database() {
    //Given
    let client = SqliteDatabase::new_in_memory().init().unwrap();
    //Initialize database store (empty)
    let bank_transaction_id = "something".to_string();
    let domain_transaction_id = "something-else".to_string();
    let transaction_time = DateTime::parse_from_rfc3339(STABLE_DATETIME).unwrap().into();
    client
        .insert_if_new(
            bank_transaction_id.clone(),
            domain_transaction_id.clone(),
            &transaction_time,
        )
        .await
        .unwrap();

    //When
    // When we try to insert an existing forensic
    client
        .insert_if_new(
            bank_transaction_id.clone(),
            domain_transaction_id.clone(),
            &transaction_time,
        )
        .await
        .unwrap();

    //Then
    // There should still only be one record in the database for the id pair
    let added_count = client.forensics_record_count(bank_transaction_id.as_str(), domain_transaction_id.as_str());
    assert_eq!(added_count, 1);
}

#[tokio::test]
async fn insert_if_new__adds_records_with_same_domain_txn_id_but_different_bank_txn_id() {
    //Given
    let client = SqliteDatabase::new_in_memory().init().unwrap();
    // Initialize database store (empty)
    let bank_transaction_id = "something".to_string();
    let bank_transaction_id2 = "another-thing".to_string();
    let domain_transaction_id = "something-else".to_string();
    let transaction_time = DateTime::parse_from_rfc3339(STABLE_DATETIME).unwrap().into();
    // Add the first forensic using the first bank transaction ID
    client
        .insert_if_new(
            bank_transaction_id.clone(),
            domain_transaction_id.clone(),
            &transaction_time,
        )
        .await
        .unwrap();

    //When
    // When we add a similar forensic with a different bank transaction id
    client
        .insert_if_new(
            bank_transaction_id2.clone(),
            domain_transaction_id.clone(),
            &transaction_time,
        )
        .await
        .unwrap();

    //Then
    // We should be able to see both transactions in the history
    let count1 = client.forensics_record_count(bank_transaction_id.as_str(), domain_transaction_id.clone().as_str());
    let count2 = client.forensics_record_count(bank_transaction_id2.as_str(), domain_transaction_id.as_str());
    assert_eq!(count1, 1);
    assert_eq!(count2, 1);
}
#[tokio::test]
async fn insert_if_new__adds_records_with_same_bank_txn_id_but_different_domain_txn_id() {
    //Given
    let client = SqliteDatabase::new_in_memory().init().unwrap();
    // Initialize database store (empty)
    let bank_transaction_id = "something".to_string();
    let domain_transaction_id = "something-else".to_string();
    let domain_transaction_id2 = "another-thing".to_string();
    let transaction_time = DateTime::parse_from_rfc3339(STABLE_DATETIME).unwrap().into();
    // Add the first forensic using the first bank transaction ID
    client
        .insert_if_new(
            bank_transaction_id.clone(),
            domain_transaction_id.clone(),
            &transaction_time,
        )
        .await
        .unwrap();

    //When
    // When we add a similar forensic with a different bank transaction id
    client
        .insert_if_new(
            bank_transaction_id.clone(),
            domain_transaction_id2.clone(),
            &transaction_time,
        )
        .await
        .unwrap();

    //Then
    // We should be able to see both transactions in the history
    let count1 = client.forensics_record_count(bank_transaction_id.clone().as_str(), domain_transaction_id.as_str());
    let count2 = client.forensics_record_count(bank_transaction_id.as_str(), domain_transaction_id2.as_str());
    assert_eq!(count1, 1);
    assert_eq!(count2, 1);
}
