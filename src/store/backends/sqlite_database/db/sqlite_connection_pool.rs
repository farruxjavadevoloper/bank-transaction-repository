use crate::store::backends::sqlite_database::db::error::database::{Error as DatabaseError, Result as DatabaseResult};
use diesel::{
    r2d2::{ConnectionManager, Pool, PooledConnection},
    Connection as DieselConnection, SqliteConnection,
};
use std::fmt::{Debug, Formatter};

#[derive(Clone)]
pub struct SqliteConnectionPool {
    inner: Pool<ConnectionManager<SqliteConnection>>,
}

impl Debug for SqliteConnectionPool {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        self.inner.state().fmt(f)
    }
}

impl SqliteConnectionPool {
    /// Requests connection from self's pool for running closure.
    /// Ensures that connections are released to the Pool when queries are complete.
    pub fn connect_and_run<F, R>(&self, f: F) -> DatabaseResult<R>
    where
        F: FnOnce(&Connection) -> DatabaseResult<R>,
    {
        let conn = self
            .inner
            .get()
            .map_err(|e| DatabaseError::ConnectionError(Box::new(e)))?;

        // turn on foreign key enforcement and enable Write-Ahead Logging (https://sqlite.org/wal.html) for better consistency
        conn.execute("PRAGMA foreign_keys = ON; PRAGMA journal_mode=WAL;")
            .map_err(|e| DatabaseError::ConnectionError(Box::new(e)))?;
        f(&conn)
    }
}

impl From<Pool<ConnectionManager<SqliteConnection>>> for SqliteConnectionPool {
    fn from(inner: Pool<ConnectionManager<SqliteConnection>>) -> Self {
        Self { inner }
    }
}

/// Type alias for simplifying references to underlying diesel type
pub type Connection = PooledConnection<ConnectionManager<diesel::SqliteConnection>>;
