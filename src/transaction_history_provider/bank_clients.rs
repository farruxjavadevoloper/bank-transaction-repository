use crate::{
    ingestor::xand_banks_bank_client::{error::Result, XandBanksBankClient},
    public_interface::BankClientConfiguration,
    transaction_history_provider::BankClient,
};
use std::sync::Arc;
use xand_secrets::SecretKeyValueStore;

pub fn construct_bank_client(
    client_config: &BankClientConfiguration,
    secret_store: Arc<dyn SecretKeyValueStore>,
) -> Result<Box<dyn BankClient>> {
    // TODO: A BankClient should be constructor dependency injected into the public interface
    //  https://dev.azure.com/transparentsystems/xand-network/_boards/board/t/Deliverance/Backlog%20items/?workitem=7154
    Ok(match client_config {
        #[cfg(test)]
        BankClientConfiguration::Test1 | BankClientConfiguration::Test2 => Box::new(
            crate::ingestor::spy_bank_client::SpyBankClient::new_with_config(client_config.clone()),
        ),
        _ => Box::new(XandBanksBankClient::new(client_config, secret_store)?),
    })
}
