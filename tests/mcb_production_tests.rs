#![cfg(all(test, feature = "prod-integ"))]

use crate::test_utils::{delete_database, initialize_test_btr_with};
use bank_transaction_repository::{BankAccount, HistoryRequest, SqliteDatabase, Transaction, TransactionDirection};
use chrono::DateTime;
use reqwest::header::{HeaderMap, CONTENT_TYPE};
use serde::Deserialize;
use serde_json::Value;
use std::str::FromStr;
use xand_money::{Money, Usd};

#[allow(dead_code)]
mod test_utils;

const DATE_IN_THE_PAST: &str = "2019-10-12T07:20:50.52Z";
const TRUST_ROUTING: &str = "121141343";
const MCB_GET_ACCESS_TOKEN_URL: &str = "https://cap.mcbankny.com/invoke/pub.oauth:getAccessToken";
const MCB_ACCESS_TOKEN_REQUEST_BODY: &str = "grant_type=client_credentials&scope=ScopeTS";
const MCB_IFX_ACCOUNT_TRANSACTIONS_URL: &str = "https://cap.mcbankny.com/rest/IFX_AcctTrn_v2_4.Pub";

/// Sensitive details that are dynamically loaded from environment variables at runtime.
struct Context {
    #[allow(dead_code)]
    org_id: String,
    trust_account: String,
    mcb_trust_username: String,
    mcb_trust_password: String,
}

impl Context {
    fn new() -> Self {
        let org_id = std::env::var("ORG_ID")
            .expect("Expected the ORG_ID environment variable to be set to Transparent's production MCB org id.");
        let trust_account = std::env::var("TRUST_ACCOUNT").expect("Expected the TRUST_ACCOUNT environment variable to be set to Transparent's production MCB trust account number.");
        let mcb_trust_username = std::env::var("MCB_TRUST_USERNAME").expect(
            "Expected the MCB_TRUST_USERNAME environment variable to be set to Transparent's production MCB username.",
        );
        let mcb_trust_password = std::env::var("MCB_TRUST_PASSWORD").expect(
            "Expected the MCB_TRUST_PASSWORD environment variable to be set to Transparent's production MCB password.",
        );

        Self {
            org_id,
            trust_account,
            mcb_trust_username,
            mcb_trust_password,
        }
    }

    async fn btr_db_transactions(&self) -> Vec<Transaction> {
        // Delete BTR database file if it already exists
        delete_database();

        // Instantiate BTR and do a history pull
        let btr = initialize_test_btr_with("mcb_production_config.yaml");
        let trust_bank_account = BankAccount::new(TRUST_ROUTING.to_string(), self.trust_account.to_string());
        let history_request = HistoryRequest::new(
            trust_bank_account,
            DateTime::parse_from_rfc3339(DATE_IN_THE_PAST).unwrap().into(),
        );
        btr.get_history(&history_request).await.unwrap();

        // Load transactions from DB
        let db = SqliteDatabase::open_with_file("tests/config/database/prod_integ_transaction_store.sqlite3");
        let db_client = db.init().unwrap();
        db_client
            .get_transactions_for(&BankAccount::new(TRUST_ROUTING.to_string(), self.trust_account.to_string()).into())
            .unwrap()
            .into_iter()
            .map(Transaction::from)
            .collect()
    }

    async fn mcb_api_transactions(&self) -> Vec<Transaction> {
        let http_client = reqwest::Client::builder().build().unwrap();

        let mut mcb_access_token_request_headers = HeaderMap::new();
        mcb_access_token_request_headers.insert(CONTENT_TYPE, "application/x-www-form-urlencoded".parse().unwrap());

        let access_token_request = http_client
            .post(MCB_GET_ACCESS_TOKEN_URL)
            .basic_auth(&self.mcb_trust_username, Some(&self.mcb_trust_password))
            .headers(mcb_access_token_request_headers)
            .body(MCB_ACCESS_TOKEN_REQUEST_BODY)
            .build()
            .unwrap();

        let access_token_response = http_client.execute(access_token_request).await.unwrap();
        let access_token = access_token_response.json::<McbAccessToken>().await.unwrap();

        let mut mcb_transactions_request_headers = HeaderMap::new();
        mcb_transactions_request_headers.insert(CONTENT_TYPE, "application/json".parse().unwrap());

        let mcb_request_body_path = format!(
            "{}/tests/mcb_acct_trn_body.txt",
            std::env::var("CARGO_MANIFEST_DIR").unwrap()
        );
        let mcb_ifx_account_transactions_body =
            String::from_utf8(std::fs::read(mcb_request_body_path).unwrap()).unwrap();
        let transactions_request = http_client
            .post(MCB_IFX_ACCOUNT_TRANSACTIONS_URL)
            .bearer_auth(access_token.access_token.as_str())
            .headers(mcb_transactions_request_headers)
            .body(mcb_ifx_account_transactions_body)
            .build()
            .unwrap();

        let transactions_response = http_client.execute(transactions_request).await.unwrap();
        let transactions_response_json = transactions_response.text().await.unwrap();

        parse_acct_trn_response(&transactions_response_json)
            .into_iter()
            .filter(|txn| txn.get_credit_or_debit() == TransactionDirection::Credit)
            .collect()
    }
}

#[allow(dead_code)] // Unread fields token_type and expires_in should still be represented accurately for serialization
#[derive(Deserialize, Debug)]
struct McbAccessToken {
    access_token: String,
    token_type: String,
    expires_in: u32,
}

fn parse_acct_trn_response(response: &str) -> Vec<Transaction> {
    const ACCT_TRN_INQ_RS: &str = "AcctTrnInqRs";
    const ACCT_TRN_REC: &str = "AcctTrnRec";
    const ACCT_TRN_INFO: &str = "AcctTrnInfo";
    const TRN_AMT: &str = "TrnAmt";
    const AMT: &str = "Amt";
    const DR_CR_TYPE: &str = "DrCrType";
    const DESC: &str = "Desc";
    let json_value: Value = serde_json::from_str(response).unwrap();
    json_value
        .as_object()
        .unwrap()
        .get(ACCT_TRN_INQ_RS)
        .unwrap()
        .as_object()
        .unwrap()
        .get(ACCT_TRN_REC)
        .unwrap()
        .as_array()
        .unwrap()
        .iter()
        .map(|acct_trn_rec| {
            let acct_trn_rec_object = acct_trn_rec.as_object().unwrap();
            let acct_trn_info_object = acct_trn_rec_object.get(ACCT_TRN_INFO).unwrap();
            let amount = acct_trn_info_object
                .get(TRN_AMT)
                .unwrap()
                .as_object()
                .unwrap()
                .get(AMT)
                .unwrap()
                .as_str()
                .unwrap();
            let credit_or_debit = acct_trn_info_object.get(DR_CR_TYPE).unwrap().as_str().unwrap();
            let metadata = acct_trn_info_object.get(DESC).unwrap().as_str().unwrap();

            Transaction::new(
                Usd::from_str(amount).unwrap(),
                TransactionDirection::from_str(credit_or_debit).unwrap(),
                metadata.to_string(),
            )
        })
        .collect()
}

/// This test requests the MCB transaction history for the trust account and compares the relevant
/// transaction information with that of the transactions pulled and stored by an instance of BTR.
/// Note: BTR will pull all of the existing transactions from the MCB production account because it
/// uses xand_banks to query the bank API. MCB's /AcctTrn endpoint only returns a limited number of
/// transactions and has a paging mechanism that requires subsequent calls to the endpoint. xand_banks
/// implements this, but this test only makes a single request to the endpoint. As a result, this test
/// asserts that the transactions returned by MCB are a subset of those contained in the BTR database
/// as opposed to comparing the sets for equality.
#[tokio::test]
async fn btr_stores_mcb_credit_transactions() {
    let context = Context::new();
    let mcb_txns = context.mcb_api_transactions().await;
    let btr_txns = context.btr_db_transactions().await;

    // Assert transactions in the database with those returned directly from MCB
    assert!(mcb_txns.iter().all(|txn| btr_txns.contains(txn)));
}
